new modulator.Module("voxel_main_menu", "voxel_main_menu", (function() {
    casey.run_loop = true;

    // Placeholder for the ui system //

    let menu_html = `
<div style="display:flex; max-width:200pt; max-height:400pt; margin:auto; flex-direction:column" id="main_menu">
    <input type="button" value="Load Map (Test)" style="margin-bottom:5pt;" onclick="casey.run_loop = false;">
    <div class="vmenu-expanding" style="color : #FFFF; margin-bottom:4pt;">
        <input type="button" value="Options"style="width:100%">
        <h3>Graphics Settings</h3>
        <hr>
        <div style="width : 100pt; margin : auto; margin-top:3pt;">
          <input type="checkbox" name="shadows_enabled" autocomplete="off" checked align="center"
                 onclick="casey.shadows_enabled = this.checked; nix.io.putf('[settings] Toggling shadows\\n');">
          <label for="shadows_enabled">Enable Shadows</label>
        </div>
        <div style="width : 100pt; margin : auto">
          <input type="checkbox" name="skybox_enabled" autocomplete="off" checked align="center"
                 onclick="casey.skybox_enabled = this.checked; nix.io.putf('[settings] Toggling skybox\\n');">
          <label for="skybox_enabled">Enable Skybox</label>
        </div>
        <div style="width : 100pt; margin : auto">
          <input type="checkbox" name="wireframe_enabled" autocomplete="off" align="center"
                 onclick="casey.wireframe = this.checked; nix.io.putf('[settings] Toggling wireframe rendering\\n');">
          <label for="wireframe_enabled">Enable Wireframe</label>
        </div>
        <div style="width : 100pt; margin : 3pt;">
          <input type="number" name="bloom_level" autocomplete="off" align="center" value="4" style="width : 20pt;"
                 onchange="casey.bloom_level = this.value; nix.io.putf('[settings] Bloom : %\\n', casey.bloom_level);">
          <label for="bloom_level">Bloom Level</label>
        </div>
        <h3>Player Settings</h3>
        <hr>
        <div style="width : 100pt; margin : 3pt;">
          <input type="number" name="mine_level" autocomplete="off" align="center" value="3" style="width : 20pt;"
                 onchange="casey.mine_level = this.value; nix.io.putf('[settings] Level : %\\n', casey.mine_level);">
          <label for="mine_level">Mine Level</label>
        </div>
        <div style="width : 100pt; margin : auto">
          <input type="checkbox" name="build_enabled" autocomplete="off" align="center"
                 onclick="casey.voxel_build_tool = this.checked; nix.io.putf('[settings] Toggling build mode\\n');">
          <label for="build_enabled">Build Mode</label>
        </div>
    </div>
    <input type="button" value="Edit Character">
</div>
    `;

    let menu_style = `
.vmenu-expanding {
    height   : 20pt;
    overflow : hidden;

    transition-duration : 0.5s;
}

.vmenu-expanding:hover {
    height : 200pt;
}
`;

    document.body.innerHTML += menu_html;
    document.head.innerHTML += "<style id=\"menu_style\">" + menu_style + "</style>";

    let screen = document.getElementById("screen");
    let gl     = screen.context;

    screen.resize(screen.clientWidth, screen.clientHeight)

    // Initialize the backround animation thingy //
    let scr_verts = new Float32Array(0.0,0.0, 1.0,0.0, 1.0,1.0, 1.0,0.0);

    let frame = 0;

    let HSLtoRGB = function(h, s, l) { // Thanks stackoverflow
        var r, g, b;

        if(s == 0){
            r = g = b = l; // achromatic
        }else{
            var hue2rgb = function hue2rgb(p, q, t){
                if(t < 0) t += 1;
                if(t > 1) t -= 1;
                if(t < 1/6) return p + (q - p) * 6 * t;
                if(t < 1/2) return q;
                if(t < 2/3) return p + (q - p) * (2/3 - t) * 6;
                return p;
            }

            var q = l < 0.5 ? l * (1 + s) : l + s - l * s;
            var p = 2 * l - q;
            r = hue2rgb(p, q, h + 1/3);
            g = hue2rgb(p, q, h);
            b = hue2rgb(p, q, h - 1/3);
        }

        return [Math.round(r * 255), Math.round(g * 255), Math.round(b * 255)];
    }

    let loop = function() {
        frame++;

        let rgb = HSLtoRGB(Math.sin(frame/200.0), 0.5, 0.2);
        nix::gl::clear_with_color(gl, rgb[0]/255, rgb[1]/255, rgb[2]/255, 1.0);

        if(casey.run_loop) {
            requestAnimationFrame(loop);
        } else {
            document.getElementById("main_menu").remove();
            document.getElementById("menu_style").remove();

            main.voxel_test();
        }
    }

    requestAnimationFrame(loop);

    return SUCCESS;
}));