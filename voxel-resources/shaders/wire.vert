#version 300 es

uniform mat4 mvp;  // Model * View * Projection

uniform float viewport_width;

layout(location = 0) in vec3 pos;
layout(location = 1) in vec2 luv;
layout(location = 2) in vec3 lnorm;
out vec2 uv;
out vec3 norm;
flat out int face;

void main() {
    ivec3 anorm = ivec3(abs(norm));

    if(anorm.z > anorm.x && anorm.z > anorm.y) {
        face = 0;
    } else if(anorm.x > anorm.y) {
        face = 1;
    } else {
        face = 2;
    }

    gl_Position = mvp*vec4(pos, 1.0);
    uv = luv;
    norm = lnorm;
}