#version 300 es

in  vec2 pos;
out vec2 uv;

void main() {
    gl_Position = vec4(pos, 0.0, 1.0);
    uv = (pos + vec2(1.0, 1.0)) / vec2(2.0);
}
