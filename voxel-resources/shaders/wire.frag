#version 300 es

precision mediump float;

uniform sampler2D fb_in;

layout (location = 0) out vec4 frag;
//layout (location = 1) out vec4 spec;

in vec2 uv;
in vec3 norm;
flat in int face;

const float F_BEND_STRENGTH=5.0;
const float F_BEND_DENSITY=20.0;

void main() {
    vec3 p_bend = refract(vec3(sin(uv.x*F_BEND_DENSITY)*F_BEND_STRENGTH, 0.0, cos(uv.x*F_BEND_DENSITY)*F_BEND_STRENGTH),
                          norm, 0.5);

    vec2 bend;
    switch(face) {
        case 0:
            bend = p_bend.xy;
            break;
        case 1:
            bend = p_bend.zy;
            break;
        case 2:
            bend = p_bend.xz;
            break;
    }

    vec3 bent_tex = texture(fb_in, vec2(gl_FragCoord.x+bend.x, gl_FragCoord.y+bend.y)/vec2(textureSize(fb_in, 0))).xyz;

    frag = vec4(bent_tex * ((bend.y/16.0)+(1.0+(1.0/16.0))), 1.0);
    //spec = vec4(0.0, 0.0, 0.0, 1.0);
}
