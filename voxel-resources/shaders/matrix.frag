#version 300 es

precision mediump float;
precision mediump sampler2DShadow;

uniform sampler2DShadow shadowmap;
uniform sampler2D       block;
uniform vec3            sundir;

layout (location = 0) out vec4 frag;
layout (location = 1) out vec4 spec;

in  vec4 shadowCoord;
in  vec3 norm;
in  vec2 uv;
in  float eye_dist;

const float fog_max   = 380.0;
const float fog_min   = 180.0;
const vec3  fog_color = vec3(1.0, 1.0, 1.0);

const float light_max  = 64.0;
const float light_min  = 0.0;
const float rval       = (1.0 / 256.0);
const vec3 light_color = vec3(rval * 247.0, rval * 230.0, rval * 155.0);
const float light_intensity = 0.15;

const vec2 poissonDisk[64] = vec2[](
    vec2(-0.613392,  0.617481), vec2( 0.170019, -0.040254), vec2(-0.299417,  0.791925),
    vec2( 0.645680,  0.493210), vec2(-0.651784,  0.717887), vec2( 0.421003,  0.027070),
    vec2(-0.817194, -0.271096), vec2(-0.705374, -0.668203), vec2( 0.977050, -0.108615),
    vec2( 0.063326,  0.142369), vec2( 0.203528,  0.214331), vec2(-0.667531,  0.326090),
    vec2(-0.098422, -0.295755), vec2(-0.885922,  0.215369), vec2( 0.566637,  0.605213),
    vec2( 0.039766, -0.396100), vec2( 0.751946,  0.453352), vec2( 0.078707, -0.715323),
    vec2(-0.075838, -0.529344), vec2( 0.724479, -0.580798), vec2( 0.222999, -0.215125),
    vec2(-0.467574, -0.405438), vec2(-0.248268, -0.814753), vec2( 0.354411, -0.887570),
    vec2( 0.175817,  0.382366), vec2( 0.487472, -0.063082), vec2(-0.084078,  0.898312),
    vec2( 0.488876, -0.783441), vec2( 0.470016,  0.217933), vec2(-0.696890, -0.549791),
    vec2(-0.149693,  0.605762), vec2( 0.034211,  0.979980), vec2( 0.503098, -0.308878),
    vec2(-0.016205, -0.872921), vec2( 0.385784, -0.393902), vec2(-0.146886, -0.859249),
    vec2( 0.643361,  0.164098), vec2( 0.634388, -0.049471), vec2(-0.688894,  0.007843),
    vec2( 0.464034, -0.188818), vec2(-0.440840,  0.137486), vec2( 0.364483,  0.511704),
    vec2( 0.034028,  0.325968), vec2( 0.099094, -0.308023), vec2( 0.693960, -0.366253),
    vec2( 0.678884, -0.204688), vec2( 0.001801,  0.780328), vec2( 0.145177, -0.898984),
    vec2( 0.062655, -0.611866), vec2( 0.315226, -0.604297), vec2(-0.780145,  0.486251),
    vec2(-0.371868,  0.882138), vec2( 0.200476,  0.494430), vec2(-0.494552, -0.711051),
    vec2( 0.612476,  0.705252), vec2(-0.578845, -0.768792), vec2(-0.772454, -0.090976),
    vec2( 0.504440,  0.372295), vec2( 0.155736,  0.065157), vec2( 0.391522,  0.849605),
    vec2(-0.620106, -0.328104), vec2( 0.789239, -0.419965), vec2(-0.545396,  0.538133),
    vec2(-0.178564, -0.596057)
);

/*float random(in vec4 seed) {
    float dot_product = dot(seed, vec4(12.9898, 78.233, 45.164, 94.673));
    return fract(sin(dot_product) * 43758.5453);
}*/

float line_width = 0.10;

void main() {
    float visibility = 1.0;

    for(int i=16; i<32; ++i) {
        //int index = int(16.0 * random(vec4(uv.xyy, i))) % 16;
        if(texture(shadowmap, vec3(shadowCoord.xy + poissonDisk[i]/3000.0, (shadowCoord.z)/shadowCoord.w)) < shadowCoord.z) {
            visibility -= 0.06;
        }
    }

    float fog_factor = clamp((fog_max - eye_dist) /
                       (fog_max - fog_min), 0.0, 1.0);

    float light_factor = clamp((light_max - eye_dist) /
                         (light_max - light_min), 0.0, 1.0) * light_intensity;

    //visibility *= clamp(dot(norm, normalize(vec3(-15.0, 24.0, -15.0))), visibility / 2.0, visibility * 2.0);
    visibility = clamp(visibility + light_factor, 0.0, 1.0);

    vec3 o_color = texture(block, uv).xyz;
    //vec3 o_color = (norm + vec3(1.0)) / 2.0;

    vec4 final_color = vec4(mix((o_color * visibility), fog_color, 1.0 - fog_factor) + (light_color * light_factor / 4.0), 1.0);
    frag = final_color;

    if(dot(o_color.rgb * visibility, vec3(0.2126, 0.7152, 0.0722)) > 0.8) {
        spec = vec4(o_color.rgb, 1.0);
    } else {
        spec = vec4(0.0, 0.0, 0.0, 1.0);
    }
}
