#version 100

uniform mat4 mvp;  // Model * View * Projection
uniform mat4 bias; // Depth texture bias
uniform vec3 eyepos;
uniform vec3 cpos;

attribute vec3 pos;
attribute vec2 uvp;

varying vec2 uv;
varying vec4 shadowCoord;
varying float eye_dist;

void main() {
    vec3 matpos = pos + cpos;
    eye_dist = sqrt((matpos.x - eyepos.x) *
                    (matpos.x - eyepos.x) +
                    (matpos.y - eyepos.y) *
                    (matpos.y - eyepos.y) +
                    (matpos.z - eyepos.z) *
                    (matpos.z - eyepos.z));

    uv = uvp;
    shadowCoord = bias*vec4(matpos, 1.0);

    gl_Position = mvp*vec4(pos, 1.0);
}
