#version 300 es

uniform mat4 mvp;  // Model * View * Projection
uniform mat4 bias; // Depth texture bias
uniform vec3 eyepos;
uniform vec3 cpos;

layout(location = 0) in vec3 pos;
layout(location = 1) in vec2 uvp;
layout(location = 2) in vec3 normal;
out vec2 uv;
out vec3 norm;
out vec4 shadowCoord;
out float eye_dist;

void main() {
    vec3 matpos = pos + cpos;
    eye_dist = sqrt((matpos.x - eyepos.x) *
                    (matpos.x - eyepos.x) +
                    (matpos.y - eyepos.y) *
                    (matpos.y - eyepos.y) +
                    (matpos.z - eyepos.z) *
                    (matpos.z - eyepos.z));

    uv   = uvp;
    norm = normal;
    shadowCoord = bias*vec4(matpos, 1.0);

    gl_Position = mvp*vec4(pos, 1.0);
}
