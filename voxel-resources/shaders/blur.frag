#version 300 es

precision mediump float;

out vec4 frag;
in  vec2 uv;

uniform sampler2D tex;

uniform bool  horizontal;
const float weight[5] = float[] (0.257027, 0.1985946, 0.1216216, 0.054054, 0.016216);

void main() {
    vec2 tex_offset = 1.0 / vec2(textureSize(tex, 0));
    vec3 result     = texture(tex, uv).rgb * weight[0];

    if(horizontal) {
        for(int i = 1; i < 5; ++i) {
            result += texture(tex, uv + vec2(tex_offset.x * float(i), 0.0)).rgb * weight[i];
            result += texture(tex, uv - vec2(tex_offset.x * float(i), 0.0)).rgb * weight[i];
        }
    } else {
        for(int i = 1; i < 5; ++i) {
            result += texture(tex, uv + vec2(0.0, tex_offset.y * float(i))).rgb * weight[i];
            result += texture(tex, uv - vec2(0.0, tex_offset.y * float(i))).rgb * weight[i];
        }
    }

    frag = vec4(result, 1.0);
}