#version 300 es

in  vec3 pos;
out vec3 uv;

uniform mat4 mvp;

void main() {
    gl_Position = mvp*vec4(pos, 1.0);
    gl_Position.z = gl_Position.w - 0.00001;
    uv = pos;
}
