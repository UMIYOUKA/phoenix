#version 300 es

precision mediump float;

out vec4 frag;
in  vec2 uv;

uniform sampler2D tex;
uniform sampler2D bright;

const float F_VIGNETTE_LENSE_VERTICAL_STRETCH=0.3;
const float F_VIGNETTE_LENSE_SAFE_AREA=0.35;
const float F_VIGNETTE_STRENGTH=1.125;

const float F_CHROMATIC_ABERRATION_STRENGTH=8.0;
const float F_CHROMATIC_ABERRATION_SAFE_AREA=0.2;

const float F_GAMMA       = 1.8;
const float F_EXPOSURE    = 1.3;
const float F_SATURATION_BOOST=1.25;
const float F_HUE_SHIFT   = 1.0;
const float F_VALUE_SHIFT = 1.05; // Similar effect to exposure but later in the process

// HSV <-> RGB from https://gamedev.stackexchange.com/questions/59797/glsl-shader-change-hue-saturation-brightness

vec3 rgb2hsv(vec3 c) {
    vec4 K = vec4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
    vec4 p = mix(vec4(c.bg, K.wz), vec4(c.gb, K.xy), step(c.b, c.g));
    vec4 q = mix(vec4(p.xyw, c.r), vec4(c.r, p.yzx), step(p.x, c.r));

    float d = q.x - min(q.w, q.y);
    float e = 1.0e-10;
    return vec3(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
}

vec3 hsv2rgb(vec3 c) {
    vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
    vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
    return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

// END OF HSV

void main() {
    //frag = vec4(uv, 1.0, 1.0);
    //frag = texture(tex, uv);

    //                      //
    // CHROMATIC ABERRATION //
    //                      //

    // Fake a lense like look by using distance as the exponent in pow() //
    float dst = pow(2.0, clamp(distance(vec2(0.5, 0.5), uv) - F_CHROMATIC_ABERRATION_SAFE_AREA, 0.0, 90.0) * F_CHROMATIC_ABERRATION_STRENGTH) - 1.0;

    // Split each color channel off by an offset //
    vec4 rColor = texture(tex, uv + vec2(0.001 * dst, 0.001 * dst));
    vec4 bColor = texture(tex, uv + vec2(-0.001 * dst, 0.001 * dst));
    vec4 gColor = texture(tex, uv + vec2(-0.00001 * dst, -0.001 * dst));

    vec3 col_chroma = vec3(rColor.x, bColor.y, gColor.z);

    // VIGNETTE //

    float v_mod = (1.0 - smoothstep(0.0, 1.0, distance(vec2(0.5, 0.5),(uv / vec2(1.0, 1.0 + F_VIGNETTE_LENSE_VERTICAL_STRETCH)) + vec2(0.0, F_VIGNETTE_LENSE_VERTICAL_STRETCH / 2.0)) * F_VIGNETTE_STRENGTH - F_VIGNETTE_LENSE_SAFE_AREA));

    vec4 b_val = texture(bright, uv + vec2(-0.00005 * dst, -0.0005 * dst));

    vec3 final = pow(vec3(1.0) - exp(-(col_chroma + b_val.rgb) * F_EXPOSURE), vec3(1.0 / F_GAMMA)) * vec3(v_mod);
    vec3 hsv_final = rgb2hsv(final);
    hsv_final *= vec3(F_HUE_SHIFT, F_SATURATION_BOOST, F_VALUE_SHIFT);
    final = hsv2rgb(hsv_final);

    // Final Color //
    frag = vec4(final, 1.0);
}
