new modulator.Module('nix', 'nix', (function() {

    // Create helper functions //

    ::set_global = function(name, value) {
        window[name] = value;
    };

    ::set_global_list = function() {
        for(let i=0; i<arguments.length; i+=2) {
            nix.set_global(arguments[i], arguments[i + 1]);
        }
    };

    // Initialize base modules //

    modulator.load("math.js", true,
                   "stream.js", true);

    ::io     = new nix::TextStream();
    ::io.err = new nix::TextStream();

    ::io.add_listener(function(msg) {
        console.log("[nix::io] " + msg);
    });
    ::io.err.add_listener(function(msg) {
        console.error("[nix::io::error] " + msg);
    });



    modulator.load("registry.js", true);

    ::registry = new nix::Registry();

    ::registry.set('/nix/', this);
    ::registry.set('/nix/version', phoenix.version.all);
    ::registry.set('/nix/version/major', phoenix.version.major);
    ::registry.set('/nix/version/minor', phoenix.version.minor);
    ::registry.set('/nix/version/patch', phoenix.version.patch);
    ::registry.set('/nix/io', ::io);
    ::registry.set('/nix/gl');

    // Keys grabbed from the url     //
    // Formatted like :              //
    // ?registry-keys=/cool/key:true //

    ::registry._read_url = function(a_url) {
        let url = new URL(a_url);
        let key_var = url.searchParams.get("registry-keys");

        if(!key_var)
            return;

        let keys = key_var.split(';');

        for(key of keys) {
            let path  = key.substr(0, key.indexOf(':'));
            let value = key.substr(key.indexOf(':') + 1, key.length);

            ::registry.set_full_path(path, eval(value));
        }
    }.bind(this);

    ::registry._read_url(window.location.href);

    // Load the rest of the modules //

    if(!modulator.load('console.js', true,
                       'input.js', true,
                       'resource.js', true,
                       'default_resource_handlers.js', true,
                       'gfx/gfx.js', true,
                       'voxel/voxel.js', true,
                       'ui/ui.js', true)) return false;

    // document.body.appendChild(document.createElement('phoenix-console'));
    return SUCCESS;
}));
