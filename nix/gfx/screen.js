new modulator.Module('gfx_screen', 'nix', (function(){
    ::Screen = class CE_Phoenix_Screen extends HTMLElement {
        constructor() {
            super();

            // Create a DOM shadow and start adding new elements //

            let shadow = this.attachShadow({mode : 'open'});

            let style = document.createElement('style');
            style.textContent = `
                .phoenix-screen-canvas {
                    width  : 100%;
                    height : 100%;

                    border-radius : 0pt;
                    border-style  : none;
                    border-width  : 0pt;
                }
            `;

            let canvas = document.createElement('canvas');
            canvas.setAttribute('class', 'phoenix-screen-canvas');

            this.canvas = canvas;

            this._resize_events = [];

            window.addEventListener("resize", (function(event) {
                this.resize(this.canvas.clientWidth, this.canvas.clientHeight);
            }).bind(this));

            //                                     //
            // Try and get a working WebGL context //
            //                                     //

            let available_contexts = ["webgl2", "experimental-webgl2", "webgl", "experimental-webgl"];

            // Check if user forced webgl1 support //
            if(nix::registry.exists('nix/gl/force_webgl1')) {
                if(nix::registry.get('nix/gl/force_webgl1').value) {
                    available_contexts = ["webgl", "experimental-webgl"];
                    nix::io.putf('[nix::CE_Phoenix_Screen] Forcing WebGL1 support instead of WebGL2\n');
                }
            }

            // Go throught all the named contexts and try to get at least one of them working //
            for(let context_name of available_contexts) {
                this.context = this.canvas.getContext(context_name);
                if(!this.context) {
                    nix::io.putf("[nix::CE_Phoenix_Screen] Failed to get context '%' from this.canvas!\n", context_name);
                } else {
                    this.feature_level = context_name;
                    this.context.feature_level = context_name;
                    break;
                }
            }

            nix::io.putf("[nix::CE_Phoenix_Screen] Currently using features from '%' for rendering\n", this.feature_level);

            let wanted_extensions = ["EXT_texture_filter_anisotropic",
                                    "WEBGL_lose_context",
                                    "WEBGL_depth_texture",
                                    "OES_element_index_uint",
                                    "OES_vertex_array_object",
                                    "OES_texture_float",
                                    "OES_texture_half_float",
                                    "WEBGL_debug_renderer_info"];

            nix::registry.set('/nix/gl/extensions');
            nix::registry.set('/nix/gl/disabled_extensions');

            for(let ext of wanted_extensions) {
                let extension = this.context.getExtension(ext);
                if(!extension) {
                    nix.registry.set('/nix/gl/disabled_extensions/' + ext);
                    nix.io.putf("[nix::CE_Phoenix_Screen] [%] Failed to load extension '%'\n", this.feature_level, ext);
                    continue;
                }
                nix.registry.set('/nix/gl/extensions/' + ext, extension);
                nix.io.putf("[nix::CE_Phoenix_Screen] [%] Loaded extension '%'\n", this.feature_level, ext);
            }

            nix::registry.set('/nix/gl/screen'    , this           );
            nix::registry.set('/nix/gl/context'   , this.context   );

            nix::registry.set('/nix/gl/parameters');
            let wanted_parameters = [];

            wanted_parameters = [["VENDOR" , this.context.VENDOR ], ["RENDERER", this.context.RENDERER],
                                ["VERSION", this.context.VERSION],
                                ["SHADING_LANGUAGE_VERSION", this.context.SHADING_LANGUAGE_VERSION],
                                ["GENERATE_MIPMAP_HINT"    , this.context.GENERATE_MIPMAP_HINT    ],
                                ["MAX_VIEWPORT_DIMS"       , this.context.MAX_VIEWPORT_DIMS       ],
                                ["MAX_VARYING_VECTORS"     , this.context.MAX_VARYING_VECTORS     ],
                                ["MAX_VERTEX_ATTRIBS"      , this.context.MAX_VERTEX_ATTRIBS      ],
                                ["MAX_VERTEX_TEXTURE_IMAGE_UNITS", this.context.MAX_VERTEX_TEXTURE_IMAGE_UNITS],
                                ["MAX_VERTEX_UNIFORM_VECTORS"    , this.context.MAX_VERTEX_UNIFORM_VECTORS    ],
                                ["MAX_CUBE_MAP_TEXTURE_SIZE", this.context.MAX_CUBE_MAP_TEXTURES_SIZE],
                                ["MAX_RENDERBUFFER_SIZE"    , this.context.MAX_RENDERBUFFER_SIZE     ],
                                ["MAX_TEXTURE_SIZE"         , this.context.MAX_TEXTURE_SIZE          ],
                                ["MAX_TEXTURE_IMAGE_UNITS"         , this.context.MAX_TEXTURE_IMAGE_UNITS         ],
                                ["MAX_COMBINED_TEXTURE_IMAGE_UNITS", this.context.MAX_COMBINED_TEXTURE_IMAGE_UNITS]];

            let k_anisotropy = nix::registry.get('/nix/gl/extensions/EXT_texture_filter_anisotropic');
            if(k_anisotropy) {
                wanted_parameters.push(["MAX_TEXTURE_MAX_ANISOTROPY_EXT", k_anisotropy.value.MAX_TEXTURE_MAX_ANISOTROPY_EXT]);
            }

            let k_debug_info = nix::registry.get('/nix/gl/extensions/WEBGL_debug_renderer_info');
            if(k_debug_info) {
                wanted_parameters.push(["UNMASKED_VENDOR_WEBGL"  , k_debug_info.value.UNMASKED_VENDOR_WEBGL  ]);
                wanted_parameters.push(["UNMASKED_RENDERER_WEBGL", k_debug_info.value.UNMASKED_RENDERER_WEBGL]);
            }

            for(let param of wanted_parameters) {
                if(!param) continue;

                nix::registry.set('/nix/gl/parameters/' + param[0],
                                  this.context.getParameter(param[1]));
            }

            // This shit looks like ancient writing, I love it //
            nix::io.putf('\n╒^═74%╕\n│^ 74%│\n╞^═74%╡\n','',
                         ' WebGL Renderer Debug Info','');

            let dbg_key_string = '│ <b>^ 35%</b> : <b><span style="color:%">^ 35%</span></b> │\n';

            for(let inf of nix.registry.get('/nix/gl/parameters/').keys) {
                var col = isNaN(inf.value) ? '#26B697' : '#2697B6';
                nix.io.putf(dbg_key_string, inf.name, col, inf.value);
            }

            nix::io.putf('╘^═74%╛\n\n', '');

            this.context.clearColor(0, 0, 0, 1);
            this.context.clear(this.context.COLOR_BUFFER_BIT | this.context.DEPTH_BUFFER_BIT);

            shadow.appendChild(style );
            shadow.appendChild(canvas);
        }

        resize(w, h) {
            nix::io.putf("[nix::CE_Phoenix_Screen] Setting screen resolution to %x%\n", w, h);

            this.canvas.width  = w;
            this.canvas.height = h;

            this.context.viewport(0, 0, w, h);

            for(let e of this._resize_events) {
                e(w, h);
            }
        }

        resize_to_client() {
            this.resize(this.clientWidth, this.clientHeight);
        }

        on_resize(callback) {
            this._resize_events.push(callback);
        }
    }

    customElements.define('phoenix-screen', ::Screen);

    let _nix_screen_style = document.createElement('style');
    _nix_screen_style.textContent = `
    phoenix-screen {
        display : inline-block;

        width  : 400pt;
        height : 300pt;

        border-width : 0;
        border-style : none;
    }`;

    document.head.appendChild(_nix_screen_style);

    return SUCCESS;
}));
