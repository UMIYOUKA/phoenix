new modulator.Module('gfx_atlas', 'nix', (function(){
    ::gl::Atlas = class __internal_gl_atlas extends nix::gl::Texture {
        constructor(gl, tile_res_x, tile_res_y, tile_border, tiles_x, tiles_y) {
            super(gl, gl.TEXTURE_2D);

            this.map = {
                width  : tiles_x,
                height : tiles_y,

                texture : {
                    width  : tiles_x * (tile_res_x + (tile_border * 2)),
                    height : tiles_y * (tile_res_x + (tile_border * 2)),
                }
            };

            this.map.texture.ratio = {
                horizontal : 1.0 / this.map.texture.width,
                vertical   : 1.0 / this.map.texture.height
            }


            this.tile = {
                width   : tile_res_x,
                height  : tile_res_y,

                texture : {
                    width  : tile_res_x + (tile_border * 2),
                    height : tile_res_y + (tile_border * 2),
                },

                border : tile_border,

                current : 0
            };

            this.tile.texture.ratio  = {
                border : {
                    nudge : {
                        horizontal : this.map.texture.ratio.horizontal * this.tile.border,
                        vertical   : this.map.texture.ratio.vertical   * this.tile.border
                    }
                },

                width  : this.map.texture.ratio.horizontal * this.tile.texture.width,
                height : this.map.texture.ratio.vertical   * this.tile.texture.height
            };

            this.tile.texture.size = {
                width  : this.map.texture.ratio.horizontal * this.tile.width,
                height : this.map.texture.ratio.vertical   * this.tile.height
            }

            


            this.set_parameters([nix.gl.TEX_PARAM_I, this.gl.TEXTURE_WRAP_S, this.gl.CLAMP_TO_EDGE],
                                [nix.gl.TEX_PARAM_I, this.gl.TEXTURE_WRAP_T, this.gl.CLAMP_TO_EDGE],
                                [nix.gl.TEX_PARAM_I, this.gl.TEXTURE_MIN_FILTER, this.gl.LINEAR ],
                                [nix.gl.TEX_PARAM_I, this.gl.TEXTURE_MAG_FILTER, this.gl.NEAREST]);

            this.image2D(0, this.gl.RGBA,
                            this.map.texture.width, this.map.texture.height,
                            this.gl.RGBA, this.gl.UNSIGNED_BYTE,
                            new Uint8Array(this.map.texture.width * this.map.texture.height * 4));
        }

        push_tile(data_or_pixels, tile_pos_x, tile_pos_y) {
            if(data_or_pixels instanceof Image) {
                this.subImage2D(0,
                                (this.tile.texture.width  * tile_pos_x),
                                (this.tile.texture.height * tile_pos_y),
                                this.gl.RGBA, this.gl.UNSIGNED_BYTE,
                                data_or_pixels);
            } else {
                this.subImage2D(0,
                                (this.tile.texture.width  * tile_pos_x),
                                (this.tile.texture.height * tile_pos_y),
                                this.tile.texture.width, this.tile.texture.height,
                                this.gl.RGBA, this.gl.UNSIGNED_BYTE,
                                data_or_pixels);
            }
        }

        load_tile(path, callback, x_off, y_off) {
            nix::io.putf('[nix::gl::atlas::load] Asynchronously loading tile "%"\n', path);

            let img = new Image();
            img.onload = function() {
                this.bind();

                nix::io.putf("[nix::gl::atlas::load] Tile inserted at '%'\n", this.tile.current);

                if(!x_off && !y_off) {
                    this.push_tile(img, this.tile.current, 0);
                } else {
                    this.push_tile(img, x_off, y_off);
                }
                this.tile.current += 1;

                callback ? callback() : null;
            }.bind(this);

            img.onerror = function() {
                nix::io.putf("[nix::gl::atlas::load] Failed loading tile '%'!\n", path);

                this.tile.current += 1;

                callback ? callback() : null;
            }.bind(this);

            img.src = path;
        }
    };

    nix::registry.set('/nix/resources/handlers/atlas',
        new nix::ResourceHandler("atlas",
            function(resource, loader) {
                // The advantage of using mostly JSON formats: //
                //      Everything is super easy to load       //

                let js_atlas = JSON.parse(resource.data);

                let gl_atlas = new nix::gl::Atlas(loader.gl, js_atlas.dimensions.tile_resolution[0], js_atlas.dimensions.tile_resolution[1],
                                                             js_atlas.dimensions.tile_border,
                                                             js_atlas.dimensions.tile_count[0], js_atlas.dimensions.tile_count[1]);

                for(let obj_tex of js_atlas.textures) {
                    // resource.path used instead of rootless_path because gl_atlas doesn't know //
                    //          about the selected asset path, might change later on             //
                    gl_atlas.load_tile(resource.path + obj_tex.path, null, obj_tex.position[0], obj_tex.position[1]);
                }

                return gl_atlas;
            })
    );

    return SUCCESS;
}));
