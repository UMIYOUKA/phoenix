new modulator.Module("renderpipeline", "nix", (function() {
    
    ::RenderPipelineStage = class __internal_nix_RenderPipeline {
        constructor(i_type) {
            this.type  = i_type;
            this.nodes = [];

            this.compiled = false;
            this.compiled_text = "";
        }

        compile() {
            let s_out = "";

            for(let node of this.nodes) {
                s_out += "\n" + node;
            }

            this.compiled_text = s_out;
            return SUCCESS;
        }

        add_node(f_function) {
            let s_full = f_function.toString();
            let s_body = s_full.slice(s_full.indexOf("{")+1, s_full.lastIndexOf("}"));

            this.compiled = false; // Node change requires a stage recompile
            this.nodes.push(s_body);
            return this.nodes.length - 1;
        }
    };

    ::RenderPipeline = class __internal_nix_RenderPipeline {
        constructor(gl) {
            this.gl = gl;
            this.stages = [];

            this.refs = {};

            this.compiled_pipeline = function() { };
        }

        add_stage(s_stagename) {
            let stagevar = "STAGE_" + s_stagename.toUpperCase();

            this[stagevar] = this.stages.length;
            this.stages.push(new nix::RenderPipelineStage(this[stagevar]));

            return SUCCESS;
        }

        add_refs(o_refs) {
            this.refs[o_refs.domain] = this.refs[o_refs.domain] ? {...this.refs[o_refs.domain], ...o_refs} : o_refs;
            return SUCCESS;
        }

        get_refs(s_domain) {
            return this.refs[s_domain];
        }

        insert(i_stage, f_function, o_refs) {
            // Access a stage index by [render pipeline name].STAGE_[stage name]

            if(o_refs) {
                this.add_refs(o_refs);
            }

            nix::io.putf("[nix::RenderPipeline.insert] Inserting into [%]\n", i_stage);

            return this.stages.find(function(e) {return e.type == i_stage}).add_node(f_function);
        }

        compile() {
            let s_full = "// RENDERING PIPELINE COMPILED BY nix::RenderPipeline.compile() //\n";

            for(let stage of this.stages) {
                if(!stage.compiled) {
                    stage.compile();
                }

                s_full += "\n// STAGE [" + stage.type + "]" + ("\n" + stage.compiled_text);
            }

            this.compiled_pipeline = Function("gl", "ref", s_full);

            return SUCCESS;
        }

        run() {
            this.compiled_pipeline(this.gl, this.refs);
        }
    };

    return SUCCESS;
}));