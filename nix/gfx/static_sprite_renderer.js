new modulator.Module('gfx_static_sprite_renderer', 'nix', (function() {

    let tmp_vertices = [0.0,0.0,0.0, 1.0,0.0,0.0, 1.0,1.0,0.0, 0.0,1.0,0.0];
    let tmp_uvs      = [0.0,0.0    , 1.0,0.0    , 1.0,1.0    , 1.0,0.0];

    ::gl::StaticSpriteRenderer = class __nix_internal_gl_StaticSpriteRenderer {
        constructor(gl) {
            this.gl = gl;

            this.created = false;

            this.sprite_positions = []; // No sprites to start //
            this.create();
        }

        create() {
            if(this.created) {
                this.destroy();
            }

            this.vao = new nix::gl::VAO(this.gl);
            this.vao.bind();

            this.vbuffer = new nix::gl::Buffer(this.gl, new Float32Array(tmp_vertices), this.gl.ARRAY_BUFFER, this.gl.STATIC_DRAW);
        }

        destroy() {
            
        }
    };

    return SUCCESS;
}));