new modulator.Module('gfx_static_mesh', 'nix', (function(){

    ::gl::StaticMesh = class __nix_internal_gl_StaticMesh {
        constructor(gl, vertices, uvs, indices, normals) {
            this.gl = gl;

            this.created = false;

            if(arguments.length > 1) {
                this.create(vertices, uvs, indices, normals);
            }
        }

        create(vertices, uvs, indices, normals) {
            if(this.created) {
                this.destroy();
            }

            if(!normals) this.no_normals = true;

            this.uv_length     = uvs.length;
            this.vertex_length = vertices.length;
            this.index_length  = indices.length;
            if(!this.no_normals) this.normal_length = normals.length;


            this.vao = new nix::gl::VAO(this.gl);
            this.vao.bind();

            this.vertex_buffer = new nix::gl::Buffer(this.gl, new Float32Array(vertices), this.gl.ARRAY_BUFFER        , this.gl.STATIC_DRAW);
            this.uv_buffer     = new nix::gl::Buffer(this.gl, new Float32Array(uvs)     , this.gl.ARRAY_BUFFER        , this.gl.STATIC_DRAW);
            this.index_buffer  = new nix::gl::Buffer(this.gl, new Int32Array(indices)   , this.gl.ELEMENT_ARRAY_BUFFER, this.gl.STATIC_DRAW);
            if(!this.no_normals) this.normal_buffer = new nix::gl::Buffer(this.gl, new Float32Array(normals) , this.gl.ARRAY_BUFFER        , this.gl.STATIC_DRAW);

            this.gl.enableVertexAttribArray(0);
            this.gl.enableVertexAttribArray(1);
            if(!this.no_normals) this.gl.enableVertexAttribArray(2);

            this.index_buffer.bind();

            this.vertex_buffer.bind();
            this.gl.vertexAttribPointer(0, 3, this.gl.FLOAT, false, 0, 0);

            this.uv_buffer.bind();
            this.gl.vertexAttribPointer(1, 2, this.gl.FLOAT, false, 0, 0);

            if(!this.no_normals) {
                this.normal_buffer.bind();
                this.gl.vertexAttribPointer(2, 3, this.gl.FLOAT, false, 0, 0);
            }
        }

        destroy() {
            this.vao.destroy();

            this.vertex_buffer.destroy();
            this.uv_buffer.destroy();
            this.index_buffer.destroy();
            if(!this.no_normals) this.normal_buffer.destroy();

            this.created = false;
        }

        bind() {
            this.vao.bind();
        }

        draw() {
            this.gl.drawElements(this.gl.TRIANGLES, this.index_length, this.gl.UNSIGNED_INT, 0);
        }

        controlled_draw(num) {
            this.gl.drawElements(this.gl.TRIANGLES, num, this.gl.UNSIGNED_INT, 0);
        }
    }

    return SUCCESS;
}));
