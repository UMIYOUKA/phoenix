new modulator.Module('gfx', 'nix', (function(){
    modulator.load('screen.js', true,
                   'gl/gl.js', true,
                   'static_mesh.js', true,
                   'primitive_meshes.js', true,
                   'atlas.js', true,
                   'renderpipeline.js', true);

    return SUCCESS;
}));