new modulator.Module('gl_program', 'nix', (function(){
    ::gl::Program = class __nix_internal_gl_Program extends ::gl::Object {
        constructor(gl, arr_shaders, b_link) {
            super(gl);

            this.uniform = {};
            this.create();

            if(arguments.length > 1) {
                this.attach_and_link(arr_shaders, b_link);
            } 
        }

        create() {
            this.destroy();

            this.shaders = [];
            this.glid    = this.gl.createProgram();

            return true;
        }

        attach_and_link(shaders, link) {
            if(!this.attach(shaders)) {
                nix::io.putf('[nix::gl::program::attach_and_link] Cannot link, shader attach failed!\n');
                return false;
            }

            if(link && this.link()) {
                return true;
            }

            nix::io.putf('[nix::gl::program::create] Heads-up, program "%" has been created without being linked\n', this.glid);
            return true;
        }

        link() {
            if(!this.glid) {
                nix::io.put('[nix::gl::program::use] Cannot link an invalid program!\n');
                return false;
            }

            this.gl.linkProgram(this.glid);

            if(this.gl.getProgramParameter(this.glid, this.gl.LINK_STATUS)) {
                return true;
            }

            nix::io.putf('[nix::gl::program::use] Failed to link program "%"\n[nix::gl::program::use] Reason : "%"\n',
                    this.glid, this.gl.getProgramInfoLog(this.glid));
            this.destroy();
            return false;
        }

        destroy() {
            if(!this.glid) return;

            this.uniform = {};
            this.gl.deleteProgram(this.glid);
            this.glid = 0;
        }

        use() {
            if(!this.glid) {
                nix::io.put('[nix::gl::program::use] Cannot use an invalid program!\n');
                return false;
            }

            this.gl.useProgram(this.glid);
            return true;
        }

        attach(shader) {
            // Detect if array and attach all in the array //
            if(Array.isArray(shader)) {
                for(let s of shader) {
                    if(!this.attach(s)) {
                        nix::io.putf("[nix::gl::program::attach] Array attach failed!\n");
                        return false;
                    }
                }

                return true;
            }

            let s = shader;

            if(typeof shader == 'string' || shader instanceof String) {
                s = new nix::gl::Shader(this.gl, shader);
            }

            if(!s.glid) {
                nix::io.putf('[nix::gl::program::attach] Cannot attach an invalid shader!\n');
                return false;
            }

            this.shaders.push(s);

            this.gl.attachShader(this.glid, s.glid);
            return true;
        }

        uniform_location(name) {
            if(!this.uniform[name]) 
                this.uniform[name] = this.gl.getUniformLocation(this.glid, name);

            return this.uniform[name];
        }
    }

    nix::registry::set('/nix/resources/handlers/prog',
        new nix::ResourceHandler("prog",
            function(resource, loader) {
                let prog_raw = JSON.parse(resource.data);
                let p_new = new nix::gl::Program(loader.gl, prog_raw.shaders.map(s_path => resource.path + s_path), true);

                for(s_uniform of prog_raw.uniform) {
                    p_new.uniform_location(s_uniform);
                }

                return p_new;
            }, true)
    );

    return SUCCESS;
}));
