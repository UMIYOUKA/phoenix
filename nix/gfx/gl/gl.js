new modulator.Module('gl', 'nix', (function() {
    modulator.lib_load('../../../libraries/gl-matrix-min.js')

    modulator.load('common.js', true,
                   'buffer.js', true,
                   'shader.js', true,
                   'program.js', true,
                   'vao.js', true,
                   'texture.js', true,
                   'framebuffer.js', true,
                   'shadowbuffer.js', true,
                   'mat4.js', true);

    return SUCCESS;
}));