new modulator.Module('gl_framebuffer', 'nix', (function(){
    ::gl::Framebuffer = class __nix_internal_gl_Framebuffer extends ::gl::Object {
        constructor(gl, type) {
            super(gl);

            if(arguments.length > 1) {
                this.create(type);
            }
        }

        create(type) {
            if(this.glid) {
                this.destroy();
            }

            this.type = type;

            this.glid = this.gl.createFramebuffer();
            this.bind();
        }

        destroy() {
            if(!this.glid) return;

            this.gl.deleteFramebuffer(this.glid);
            this.glid = 0;
        }

        bind() {
            this.gl.bindFramebuffer(this.type, this.glid);
        }

        check() {
            this.bind();
            if(gl.checkFramebufferStatus(this.type) != gl.COMPLETE) {
                nix::io.putf("[nix::gl::framebuffer] Framebuffer not complete!");
                return false;
            }
            return true;
        }

        texture(bind_type, tex, level) {
            this.bind();
            this.gl.framebufferTexture2D(this.type, bind_type, tex.type, tex.glid, level);
        }
    }

    return SUCCESS;
}));
