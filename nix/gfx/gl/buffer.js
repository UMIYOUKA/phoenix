new modulator.Module('gl_buffer', 'nix', (function(){
    ::gl::Buffer = class __nix_gl_internal_Buffer extends ::gl::Object {
        constructor(gl, data, type, draw) {
            super(gl);

            if(arguments.length > 1) {
                this.create(data, type, draw);
            }
        }

        bind() {
            this.gl.bindBuffer(this.type, this.glid);
        }

        create(data, type, draw) {
            this.destroy();

            this.type = type;
            this.draw = draw;

            this.glid = this.gl.createBuffer();
            this.bind();
            this.buffer_data(data);
        }

        destroy() {
            if(!this.glid) return;

            this.gl.deleteBuffer(this.glid);
            this.glid = 0;
        }

        buffer_data(data) {
            this.gl.bufferData(this.type, data, this.draw);
        }

        buffer_sub_data(data, offset) {
            this.gl.bufferSubData(this.type, offset, data);
        }
    }

    return SUCCESS;
}));