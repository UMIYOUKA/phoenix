new modulator.Module('gl_mat4', 'nix', (function(){
    // Makes glMatrix's matrixes actual objects, easier to program cleanly //

    ::gl::mat4 = class gl_mat4 {
        constructor(matrix) {

            if(arguments.length) {
                this.matrix = mat4.create();
                this.steal(matrix);
            } else {
                this.matrix = mat4.create();
            }

            this.mat = this.matrix;
        }

        // Initialization //

        clone() {
            return mat4.clone(this.matrix);
        }

        copy(out) {
            mat4.copy(out.mat, this.matrix);
        }

        steal(a) {
            mat4.copy(this.matrix, a.mat);
        }

        identity() {
            mat4.identity(this.matrix);
        }

        // Rotations //

        rotate(degrees, axis) {
            mat4.rotate(this.matrix, this.matrix, nix.gl.radians(degrees), axis);
        }

        rotate_x(degrees) {
            mat4.rotateX(this.matrix, this.matrix, nix.gl.radians(degrees));
        }

        rotate_y(degrees) {
            mat4.rotateY(this.matrix, this.matrix, nix.gl.radians(degrees));
        }

        rotate_z(degrees) {
            mat4.rotateZ(this.matrix, this.matrix, nix.gl.radians(degrees));
        }

        // Translations //

        translate(vector) {
            mat4.translate(this.matrix, this.matrix, vector);
        }

        transpose(a) {
            mat4.transpose(this.matrix, a.mat);
        }

        // Scales //

        scale(vector) {
            mat4.scale(this.matrix, this.matrix, vector);
        }

        // Math //

        adjoint(out) {
            mat4.adjoint(out, this.matrix);
        }

        determinant() {
            return mat4.determinant(this.matrix);
        }

        frob() {
            return mat4.frob(this.matrix);
        }

        // Matrix Math //

        ortho(left, right, bottom, top, near, far) {
            mat4.ortho(this.matrix, left, right, bottom,  top, near, far);
        }

        frustum(left, right, bottom, top, near, far) {
            mat4.frustum(this.matrix, left, right, bottom, top, near, far);
        }

        perspective(fovy, aspect, near, far) {
            mat4.perspective(this.matrix, nix.gl.radians(fovy), aspect, near, far);
        }

        perspective_from_field_of_view(fov, near, far) {
            mat4.perspectiveFromFieldOfView(this.matrix, fov, near, far);
        }

        invert(mat) {
            if(mat) {
                mat4.invert(this.matrix, mat.mat);
            } else {
                mat4.invert(this.matrix, this.matrix);
            }
        }

        look_at(eye, center, up) {
            mat4.lookAt(this.matrix, eye, center, up);
        }

        target_to(eye, center, up) {
            mat4.targetTo(this.matrix, eye, center, up);
        }

        get_rotation(quat) {
            mat4.getRotation(quat, this.matrix);
        }

        get_scaling(vector) {
            mat4.getScaling(vector, this.matrix);
        }

        get_translation(vector) {
            mat4.getTranslation(vector, this.matrix);
        }

        // Operators //

        equals(b) {
            return mat4.equals(this.matrix, b.mat);
        }

        exact_equals(b) {
            return mat4.exactEquals(this.matrix, b.mat);
        }

        add(out, b) {
            mat4.add(out.mat, this.matrix, b.mat);
        }

        sub(out, b) {
            mat4.sub(out.mat, this.matrix, b.mat);
        }

        subtract(out, b) {
            mat4.subtract(out.mat, this.matrix, b.mat);
        }

        mul(out, b) {
            mat4.mul(out.mat, this.matrix, b.mat);
        }

        multiply(out, b) {
            mat4.multiply(out.mat, this.matrix, b.mat);
        }

        multiply_scalar(out, b) {
            mat4.multiplyScalar(out.mat, this.matrix, b.mat);
        }

        multiply_scalar_and_add(out, b, scale) {
            mat4.multiplyScalarAndAdd(out.mat, this.matrix, b.mat, scale);
        }

        // Misc //

        str() {
            return mat4.str(this.matrix);
        }

        get_direction() {
            return {
                right : {
                    x : this.matrix[0],
                    y : this.matrix[4],
                    z : this.matrix[8]
                },

                left : {
                    x : -this.matrix[0],
                    y : -this.matrix[4],
                    z : -this.matrix[8]
                },

                up : {
                    x : this.matrix[1],
                    y : this.matrix[5],
                    z : this.matrix[9]
                },

                down : {
                    x : this.matrix[1],
                    y : this.matrix[5],
                    z : this.matrix[9]
                },

                forward : {
                    x : -this.matrix[2],
                    y : -this.matrix[6],
                    z : -this.matrix[10]
                },

                backward : {
                    x : this.matrix[2],
                    y : this.matrix[6],
                    z : this.matrix[10]
                }
            };
        }
    }

    return SUCCESS;
}));