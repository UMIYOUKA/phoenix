new modulator.Module('gl_common', 'nix', (function() {
    ::gl = {
        byte_to_clamp    : function(b) {
            return (1/255)*b;
        },

        clear_with_color : function(gl, r,g,b,a) {
            gl.clearColor(r,g,b,a);
            gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
        },

        radians : function(degrees) {
            return degrees*(Math.PI / 180);
        },

        distance : function(px, py, sx, sy) {
            // √( (x₂ - x₁)² + (y₂ - y₁)²)
            return Math.sqrt(Math.pow(sx-px,2)+Math.pow(sy-py,2));
        },

        toggle_arrays : function(enable, gl) {
            for(let i = 2; i < arguments.length; ++i) {
                if(enable) {
                    // nix.io.putf('[nix::gl::enable_arrays] Enabling array %\n', arguments[i]);
                    gl.enableVertexAttribArray(arguments[i]);
                } else {
                    gl.disableVertexAttribArray(arguments[i]);
                }
            }
        },

        enable_arrays : function() {
            this.toggle_arrays(true, arguments);
        },

        disable_arrays : function() {
            this.toggle_arrays(false, arguments);
        }
    };

    ::gl::Object = class __nix_internal_gl_Object {
        constructor(gl) {
            this.glid = 0;
            this.gl   = gl;
        }
    }

    return SUCCESS;
}));