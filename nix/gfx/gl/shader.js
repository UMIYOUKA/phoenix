new modulator.Module('gl_shader', 'nix', (function(){
    /*
     *          [[nix::gl::Shader]]
     * 
     * constructor(gl, path [A path like '/thingy/shader.vert'] or type)
     * 
     * - The JS OOP version of gl's shaders.
     * - Support loading shaders from a file
     * - Cannot be directly loaded by a
     *   Resource handler, load programs instead
     */

    ::gl::Shader = class __nix_internal_gl_Shader extends ::gl::Object {
        constructor(gl, path_or_type) {
            super(gl);

            // Second argument is optional //
            // To allow for more advanced  //
            // Shader stuffs like dynamic  //
            // compiling and such.         //
            if(typeof path_or_type == 'string' || path_or_type instanceof String) {
                this.load(path_or_type);
            } else if(arguments.length > 1) {
                this.create(path_or_type);
            }
        }

        source(s_src) {
            // Shader pre-processing is planned //
            // But this is fine for now.        //
            this.gl.shaderSource(this.glid, s_src);
        }

        compile() {
            // Compile the shader and return   //
            // The status. If it fails, log it //
            this.gl.compileShader(this.glid);

            if(this.gl.getShaderParameter(this.glid, this.gl.COMPILE_STATUS))
                return true;

            nix::io.putf('% Shader compilation failed!\n% getShaderInfoLog : %\n',
                          '[nix::gl::shader::compile]',
                          this.gl.getShaderInfoLog(this.glid),
                          '[nix::gl::shader::compile]');
            
            return false;
        }

        create(i_type) {
            this.destroy();

            this.type    = i_type;
            this.glid    = this.gl.createShader(this.type);

            return true;
        }

        load(s_path) {
            this.destroy();

            if(s_path == null) {
                nix::io.putf('[nix::gl::shader::load] Unable to load shader "%", path cannot be null!\n', path);
                return false;
            }

            this.path = s_path;
            if(this.gl.feature_level != "webgl2") {
                this.path = this.path.substring(0, this.path.lastIndexOf("."))
                            + ".wgl1"
                            + this.path.substring(this.path.lastIndexOf("."), this.path.length);
            }

            nix::io.putf('[nix::gl::shader::load] Loading shader "%"\n', this.path);

            // Try to figure out what kind   //
            // of shader this is, more would //
            // be supported if webgl used    //
            // compute or geometry shaders   //
            let f_end = this.path.substring(this.path.lastIndexOf(".") + 1, this.path.length);

            switch(f_end) {
                case "vert":
                case "vertex":
                case "vglsl":
                    this.type = this.gl.VERTEX_SHADER;
                    break;
                case "frag":
                case "fragment":
                case "fglsl":
                    this.type = this.gl.FRAGMENT_SHADER;
                    break;
                default:
                    nix::io.putf('[nix::gl::shader::load] Unable to load shader "%", unable to decyhper type "%"!\n',
                                this.path, f_end);
                    return false;
            }

            this.create(this.type);
            this.source(nix.file.load(this.path));

            if(!this.compile()) {
                nix::io.putf('[nix::gl::shader::load] Failed to load shader "%"!\n', this.path);
                return false;
            }

            nix::io.putf('[nix::gl::shader::load] Shader "%" Successfuly loaded!\n', this.path);
            return true;
        }

        destroy() {
            if(!this.glid) return;

            this.gl.deleteShader(this.glid);
            this.glid = 0;
        }
    }

    return SUCCESS;
}));