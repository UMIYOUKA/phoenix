new modulator.Module('gl_texture', 'nix', (function(){
    nix::gl::TEX_PARAM_I = 1;
    nix::gl::TEX_PARAM_F = 2;

    ::gl::Texture = class __nix_internal_gl_Texture extends ::gl::Object {
        constructor(gl, type) {
            super(gl);

            if(arguments.length>1) {
                this.create(type);
            }
        }

        create(type) {
            if(this.glid) {
                this.destroy();
            }

            this.type = type;

            this.glid = this.gl.createTexture();
        }

        destroy() {
            if(!this.glid) return;

            this.gl.deleteTexture(this.glid);
            this.glid = 0;
        }

        bind() {
            this.gl.bindTexture(this.type, this.glid);
        }

        set_parameters() {
            this.bind();
            for(let p of arguments) {
                switch(p[0]) {
                    case nix.gl.TEX_PARAM_I:
                        this.gl.texParameteri(this.type, p[1], p[2]);
                        continue;
                    case nix.gl.TEX_PARAM_F:
                        this.gl.texParameterf(this.type, p[1], p[2]);
                        continue;
                    default:
                        nix::io.putf('[nix::gl::texture::set_parameters] Invalid texture paramter type "%"\n', p[0]);
                        continue;
                }
            }
        }

        image2D(level, internal_format, width_or_src_format, height_or_src_type, format_or_pixels, data_type, pixels) {
            this.bind();

            if(!(format_or_pixels instanceof Image)) {
                this.width  = width_or_src_format;
                this.height = height_or_src_type;
            } else {
                this.width  = format_or_pixels.width;
                this.height = format_or_pixels.height;
            }

            if(arguments.length == 7) {
                this.gl.texImage2D(this.type, level,
                                internal_format,
                                this.width, this.height, 0,
                                format_or_pixels,
                                data_type,
                                pixels);
            } else {
                this.gl.texImage2D(this.type, level,
                                internal_format,
                                width_or_src_format,
                                height_or_src_type,
                                format_or_pixels);
            }
        }

        subImage2D(level, x_offset, y_offset, width_or_format, height_or_type, format_or_pixels, type, pixels) {
            this.bind();

            if(arguments.length == 6) {
                this.gl.texSubImage2D(this.type, level,
                                    x_offset, y_offset,
                                    width_or_format, height_or_type,
                                    format_or_pixels);
            } else {
                this.gl.texSubImage2D(this.type, level,
                                    x_offset, y_offset,
                                    width_or_format, height_or_type,
                                    format_or_pixels, type,
                                    pixels);
            }
        }

        generate_mipmaps() {
            if(!Math.powerOfTwo(this.width) && !Math.powerOfTwo(this.height)) {
                nix::io.putf('[nix::gl::texture::generate_mipmaps] Texture width and height must both be a power of two\n');
                return false;
            }

            this.gl.generateMipmap(this.type);

            this.set_parameters([nix.gl.TEX_PARAM_I, this.gl.TEXTURE_MIN_FILTER, this.gl.LINEAR_MIPMAP_LINEAR],
                                [nix.gl.TEX_PARAM_I, this.gl.TEXTURE_MAG_FILTER, this.gl.LINEAR]);

            return true;
        }

        load(path, level) {
            nix::io.putf('[nix::gl::texture::load] Asynchronously loading texture "%"\n', path);

            this.image2D((level != null) ? level : 0,
                        this.gl.RGBA, 2, 2, this.gl.RGBA, this.gl.UNSIGNED_BYTE,
                        new Uint8Array([255,0,255,255,  0,0,0,255,  0,0,0,255,  255,0,255,255]));

            this.set_parameters([nix.gl.TEX_PARAM_I, this.gl.TEXTURE_WRAP_S, this.gl.CLAMP_TO_EDGE],
                                [nix.gl.TEX_PARAM_I, this.gl.TEXTURE_WRAP_T, this.gl.CLAMP_TO_EDGE],
                                [nix.gl.TEX_PARAM_I, this.gl.TEXTURE_MIN_FILTER, this.gl.LINEAR],
                                [nix.gl.TEX_PARAM_I, this.gl.TEXTURE_MAG_FILTER, this.gl.LINEAR]);

            let img = new Image();
            img.onload = function() {
                this.bind();

                this.image2D(level, this.gl.RGBA, this.gl.RGBA, this.gl.UNSIGNED_BYTE, img);

                if(Math.powerOfTwo(this.width) && Math.powerOfTwo(this.height)) {
                    let ext = nix::registry.get('/nix/gl/extensions/EXT_texture_filter_anisotropic');
                    if(ext) {
                        this.gl.texParameterf(this.type,
                                            ext.value.TEXTURE_MAX_ANISOTROPY_EXT, 4);
                    }

                    nix::io.put('[nix::gl::texture::load] Generating Mipmaps...\n');

                    this.generate_mipmaps();
                }

                nix::io.putf('[nix::gl::texture::load] Finished loading texture "%"!\n', path);
            }.bind(this);

            img.onerror = function(e) {
                nix::io.putf('[nix::gl::texture::load] Failed to load texture "%"\n', path);
            };

            img.src = path;
        }
    };

    let rh_texhandler =
        new nix::ResourceHandler("png",
            function(resource, loader) {
                var tex = new nix::gl::Texture(loader.gl, loader.gl.TEXTURE_2D);
                tex.load(resource.full_path, 0);
                return tex;
        });

    nix::registry.set('/nix/resources/handlers/png' , rh_texhandler);
    nix::registry.set('/nix/resources/handlers/jpg' , rh_texhandler);
    nix::registry.set('/nix/resources/handlers/bmp' , rh_texhandler);
    nix::registry.set('/nix/resources/handlers/jpeg', rh_texhandler);
    nix::registry.set('/nix/resources/handlers/JPG', rh_texhandler);
    nix::registry.set('/nix/resources/handlers/PNG', rh_texhandler);
    nix::registry.set('/nix/resources/handlers/png', rh_texhandler);

    return SUCCESS;
}));