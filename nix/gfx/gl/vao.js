new modulator.Module('gl_vao', 'nix', (function(){
    ::gl::VAO = class __nix_internal_gl_VAO extends ::gl::Object {
        constructor(gl) {
            super(gl);

            if(gl.feature_level != "webgl2") {
                this.ext = nix::registry.get('/nix/gl/extensions/OES_vertex_array_object').value;
            }

            this.create();
        }

        create() {
            if(!this.ext) {
                this.glid = this.gl.createVertexArray();
                return;
            }
            this.glid = this.ext.createVertexArrayOES();
        }

        destroy() {
            if(!this.glid) return;

            if(this.ext) {
                this.ext.deleteVertexArrayOES(this.glid);
            } else {
                this.gl.deleteVertexArray(this.glid);
            }
            this.glid = 0;
        }

        bind() {
            this.gl.current_vao = this;
            if(!this.ext) {
                this.gl.bindVertexArray(this.glid);
                return
            }
            this.ext.bindVertexArrayOES(this.glid);
        }
    }

    return SUCCESS;
}));