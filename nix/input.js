new modulator.Module('input', 'nix', (function(this_module) {
    // Really ugly way to get pointer lock working //
    // thanks -moz-                                //

    ::input = {};

    this_input = ::input;

    ::input::request_mouse_lock = function(obj) { obj.requestPointerLock ? obj.requestPointerLock() : obj.mozRequestPointerLock ? obj.mozRequestPointerLock() : nix.io.put('[nix::input::request_mouse_lock] Cannot lock mouse! Pointer lock API is not supported!\n'); };
    ::input::exit_mouse_lock    = function(obj) { obj.exitPointerLock    ? obj.exitPointerLock()    : obj.mozExitPointerLock    ? obj.mozExitPointerLock()    : nix.io.put('[nix::input::exit_mouse_lock] Cannot exit mouse lock! Pointer lock API is not supported!\n'); };


    // Find out the user configured mouse deadzone (thanks firefox, for making me add this) //
    if(nix.registry.exists('/nix/input/mouse/drift')) {
        ::input.neg_drift = nix.registry.get('/nix/input/mouse/drift').value;
        nix::io.putf('[nix::input] Set mouse drift to [%]\n', ::input::neg_drift);
    } else {
        ::input::neg_drift = 0;
    }

    let __p_input_check_viability = function(obj) {
        if(obj.nix_input) {
            return;
        }

        obj.nix_input = {
            _mm_callbacks : [],
            _md_callbacks : [],
            _mu_callbacks : [],

            _kd_callbacks : [],
            _ku_callbacks : [],

            key           : []
        };

        obj.setAttribute("tabindex", 0);

        obj.onmousemove = function(e) { __p_input_call_callback(obj.nix_input._mm_callbacks, e, e.clientX, e.clientY, e.movementX < this_input.neg_drift || e.movementX > 0 ? e.movementX : 0, e.movementY < this_input.neg_drift || e.movementY > 0 ? e.movementY : 0); }
        obj.onmousedown = function(e) { __p_input_call_callback(obj.nix_input._md_callbacks, e); }
        obj.onmouseup   = function(e) { __p_input_call_callback(obj.nix_input._mu_callbacks, e); }
        obj.onkeydown   = function(e) { __p_input_call_callback(obj.nix_input._kd_callbacks, e, e.key); }
        obj.onkeyup     = function(e) { __p_input_call_callback(obj.nix_input._ku_callbacks, e, e.key); }

        this_input.on_key_down(obj, function(event, key) {
            obj.nix_input.key[key] = true;
        });

        this_input.on_key_up(obj, function(event, key) {
            obj.nix_input.key[key] = false;
        });
    }

    let __p_input_set_callback = function(arr, call) {
        arr.push(call);
        return (arr.length - 1);
    };

    let __p_input_call_callback = function(arr, e, e1, e2, e3, e4) {
        for(let c of arr) {
            c(e, e1, e2, e3, e4);
        }
    }

    ::input::on_mouse_move = function(obj, callback) { __p_input_check_viability(obj); return __p_input_set_callback(obj.nix_input._mm_callbacks, callback); }
    ::input::on_mouse_down = function(obj, callback) { __p_input_check_viability(obj); return __p_input_set_callback(obj.nix_input._md_callbacks, callback); }
    ::input::on_mouse_up   = function(obj, callback) { __p_input_check_viability(obj); return __p_input_set_callback(obj.nix_input._mu_callbacks, callback); }
    ::input::on_key_down   = function(obj, callback) { __p_input_check_viability(obj); return __p_input_set_callback(obj.nix_input._kd_callbacks, callback); }
    ::input::on_key_up     = function(obj, callback) { __p_input_check_viability(obj); return __p_input_set_callback(obj.nix_input._ku_callbacks, callback); }
    ::input::get_key       = function(obj, key)      { __p_input_check_viability(obj); return obj.nix_input.key[key]; }

    return SUCCESS;
}));
