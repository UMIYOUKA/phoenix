new modulator.Module('resource', 'nix', (function(){
    ::Resource = class _internal_resource_Resource {
        constructor(full_path) {
            this.full_path     = "";
            this.name          = "";
            this.extension     = "";
            this.path          = "";
            this.rootless_path = ""
            this.data          = null;

            if(full_path) {
                this.set(full_path);
            }
        }

        set(full_path) {
            this.full_path     = full_path;
            this.path          = full_path.substr(0, full_path.lastIndexOf('/') + 1);
            this.name          = full_path.substr(full_path.lastIndexOf('/') + 1, full_path.lastIndexOf('.'));
            this.extension     = full_path.substr(full_path.lastIndexOf('.') + 1, full_path.length);
        }

        load(loader, handle) {
            this.rootless_path = this.path.substr(loader.root.length);
            this.data = nix.file.load(this.full_path);
            if(handle) {
                return loader.handle.bind(loader)(this);
            }
            return true;
        }
    };

    ::ResourceHandler = class _internal_resource_ResourceHandler {
        constructor(extension, handle, returnable) {
            this.extension  = (extension  || "");
            this.handle     = (handle     || function(resource) { return true; });
            this.returnable = (returnable || false);
        }
    };

    ::ResourceLoader = class _internal_resource_ResourceLoader {
        constructor(root) {
            this.resources = [];
            this.root      = root || nix::registry.get('/nix/resources/default_root').value;

            this.root = (this.root[this.root.length - 1] == '/') ? (this.root) : (this.root + '/');

            this.handlers = [];
            let default_handlers = nix::registry.get('/nix/resources/handlers').keys;
            for(let handler of default_handlers) {
                this.handlers[handler.name] = handler.value;
            }
        }

        load(full_path) {
            let r = new nix::Resource(this.root + full_path);

            nix::io.putf('[nix::resource_loader] Loading resource "%" [%]\n', r.full_path, r.extension);

            let res = r.load(this, true);
            if(!res) {
                nix::io.putf('[nix::resource_loader] Failed loading resource "%" [%]\n', r.full_path, r.extension);
                return false;
            }

            nix::io.putf("[nix::resource_loader] Successfully loaded resource '%' [%]\n", r.full_path, r.extension);
            return res;
        }

        add_handler(handler) {
            this.handlers[handler.extension] = handler;
        }

        use_gl(gl) {
            this.gl = gl;
        }

        handle(resource) {
            try {
                return this.handlers[resource.extension].handle(resource, this);
            } catch(e) {
                nix::io.putf('[nix::resource_loader] Handler failed! "%"\n', e);
                return false;
            }
        }
    };

    nix::registry.set('/nix/resources');

    if(!nix::registry.exists('/nix/resources/default_root')) {
        nix::registry.set('/nix/resources/default_root', 'resources/');
    }

    return SUCCESS;
}));
