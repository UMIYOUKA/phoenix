new modulator.Module('console', 'nix', (function() {
    /*
    *  Phoenix console
    *  - mostly used for debugging stuffs
    *  - should be able to call JS functions from it
    *  - should user text streams [TODO]
    */

    ::Console = class _internal_console_Console extends HTMLElement {
        constructor() {
            super();

            if(!nix::registry.get('/nix/developer') && nix::registry.get('/nix/developer').value != true && this.getAttribute("dev") == true) {
                return; // We are not a dev, don't show the console if devonly is set
            }

            this.stream    = nix.io;
            this.stream_id = this.stream.add_listener(this.put.bind(this));

            /*
            *  Create a shadow root and add the default style, output, and input to the element
            *  Add an event listener for the enter key so it parses the input instead of just making a newline
            *  The parser just echoes what was put in + an eval to the line
            */

            let shadow = this.attachShadow({mode : 'open'});

            let style = document.createElement('style');
            style.textContent = `
                .phoenix-console-output {
                    width  : 100%;
                    height : calc(calc(100% - 2pt) - 2.5em);

                    color      : #000;
                    background : #FFFFFFDD;

                    font-family : monospace;
                    font-size   : 9pt;

                    border-radius : 0pt;
                    border-width  : 0pt;
                    border-style  : none;

                    border-bottom-width : 2pt;
                    border-bottom-style : solid;
                    border-bottom-color : #999;

                    white-space : pre;

                    overflow-y : scroll;
                    overflow-x : hidden;

                    padding-right : 20pt;

                    word-wrap : break-word;

                    margin  : 0 0 0 0;
                    padding : 0 0 0 0;

                    resize : none;
                }

                .phoenix-console-input {
                    width  : 100%;
                    height : 2.5em;

                    color      : #FFF;
                    background : #000;

                    font-family : monospace;

                    border-radius : 0pt;
                    border-width  : 0pt;
                    border-style  : none;

                    margin  : 0 0 0 0;
                    padding : 0 0 0 0;

                    resize : none;
                }
            `;

            let _this = this;

            var output = document.createElement('div');
            output.setAttribute('readonly', 'true');
            output.setAttribute('class', 'phoenix-console-output');

            output.scrollTop = output.scrollHeight;

            let input = document.createElement('textarea');
            input.setAttribute('class', 'phoenix-console-input');

            input.addEventListener('keydown', function(e) {
                if(e.keyCode == 13) {
                    _this.parse(_this.input.value);
                    _this.input.value = "";
                    e.preventDefault();
                    return false;
                }

                return true;
            }, false);

            this.output = output;
            this.input  = input;

            shadow.appendChild(style );
            shadow.appendChild(output);
            shadow.appendChild(input );
        }

        /*
        * put(text)
        * temp replacement for stringstream functions
        */

        put(text) {
            this.output.innerHTML += text;
            this.output.scrollTop = this.output.scrollHeight;
        }

        parse(text) {
            if(text == "clear") {
                this.output.innerHTML = "";
                return;
            }

            this.put(">> " + text + "\n");

            let ret = null;

            try {
                ret = eval(text);
                if(ret === Object(ret)) {
                    let s = [];
                    ret = ret.toString() + "\n" + JSON.stringify(ret, function(k,v){if(v!=null&&typeof v=="object"){if(s.indexOf(v)>=0){return;}s.push(v);}return v;}, 2) + "\n";
                }
            } catch(e) {
                this.put("[Exception] " + e + "\n");
            }

            this.put(ret + "\n");
        }
    };

    customElements.define('phoenix-console', this.Console);

    let _nix_console_style = document.createElement('style');
    _nix_console_style.textContent = `
    phoenix-console {
        display : inline-block;

        width  : 400pt;
        height : 200pt;

        padding : 0 0 0 0;
    }`;

    document.head.appendChild(_nix_console_style);

    return SUCCESS;
}));
