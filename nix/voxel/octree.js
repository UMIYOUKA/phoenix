new modulator.Module('octree', 'nix', function() {
    ::voxel::Octree = class _internal_voxel_Octree {
        constructor(v_size) {
            this.base_leaf = new nix::voxel::OctreeLeaf(this, null, 0, {x:0.0, y:0.0, z:0.0}, v_size);

            this.max_split_level = 10;
        }
    };

    ::voxel::OctreeLeaf = class _internal_voxel_OctreeLeaf {
        constructor(root, parent, split_level, v_position, v_size, neighbors) {
            this.root_octree = root;
            this.leaves      = [];
            this.solid       = false;
            this.split_level = split_level;
            this.position    = v_position;
            this.size        = v_size;
            this.parent      = parent;
            this.neighbors   = neighbors ? neighbors : {px:null,py:null,pz:null, nx:null,ny:null,nz:null};
        }

        //      X
        //   -------
        //   |2| |1|
        // Z -------
        //   |3| |0|
        //   -------

        //      X
        //   -------
        //   |6| |5|
        // Z -------
        //   |7| |4|
        //   -------

        split() {
            /*
             * Split a leaf into 8 others and increment the split level
             * to prevent the tree from getting too cluttered
             */

            this.solid = false;
            if(this.leaves.length != 8 && this.split_level < this.root_octree.max_split_level) {
                let n_size = {x:this.size.x/2.0,y:this.size.y/2.0,z:this.size.z/2.0};
                this.leaves = [new nix.voxel.OctreeLeaf(this.root_octree, this, this.split_level + 1, Math.vecAdd(this.position, {x:0       , y:0       , z:0       }), n_size),
                               new nix.voxel.OctreeLeaf(this.root_octree, this, this.split_level + 1, Math.vecAdd(this.position, {x:n_size.x, y:0       , z:0       }), n_size),
                               new nix.voxel.OctreeLeaf(this.root_octree, this, this.split_level + 1, Math.vecAdd(this.position, {x:n_size.x, y:0       , z:n_size.z}), n_size),
                               new nix.voxel.OctreeLeaf(this.root_octree, this, this.split_level + 1, Math.vecAdd(this.position, {x:0       , y:0       , z:n_size.z}), n_size),
                               new nix.voxel.OctreeLeaf(this.root_octree, this, this.split_level + 1, Math.vecAdd(this.position, {x:0       , y:n_size.y, z:0       }), n_size),
                               new nix.voxel.OctreeLeaf(this.root_octree, this, this.split_level + 1, Math.vecAdd(this.position, {x:n_size.x, y:n_size.y, z:0       }), n_size),
                               new nix.voxel.OctreeLeaf(this.root_octree, this, this.split_level + 1, Math.vecAdd(this.position, {x:n_size.x, y:n_size.y, z:n_size.z}), n_size),
                               new nix.voxel.OctreeLeaf(this.root_octree, this, this.split_level + 1, Math.vecAdd(this.position, {x:0       , y:n_size.y, z:n_size.z}), n_size)];

                this.leaves[0].neighbors = {px:this.leaves[1], py:this.leaves[4], pz:this.leaves[3], nx:null          , ny:null, nz:null};
                this.leaves[1].neighbors = {px:null          , py:this.leaves[5], pz:this.leaves[2], nx:this.leaves[0], ny:null, nz:null};
                this.leaves[2].neighbors = {px:null          , py:this.leaves[6], pz:null          , nx:this.leaves[3], ny:null, nz:this.leaves[1]};
                this.leaves[3].neighbors = {px:this.leaves[2], py:this.leaves[7], pz:null          , nx:null          , ny:null, nz:this.leaves[0]};

                this.leaves[4].neighbors = {px:this.leaves[5], py:null, pz:this.leaves[7], nx:null          , ny:this.leaves[0], nz:null};
                this.leaves[5].neighbors = {px:null          , py:null, pz:this.leaves[6], nx:this.leaves[4], ny:this.leaves[1], nz:null};
                this.leaves[6].neighbors = {px:null          , py:null, pz:null          , nx:this.leaves[7], ny:this.leaves[2], nz:this.leaves[5]};
                this.leaves[7].neighbors = {px:this.leaves[6], py:null, pz:null          , nx:null          , ny:this.leaves[3], nz:this.leaves[4]};
            }
        }

        find_leaf(v_pos) {
            /*
             * Find a leaf in the tree just from a 3D point
             */

            if(v_pos.x >=this.position.x             && v_pos.y >=this.position.y             && v_pos.z >=this.position.z&&
               v_pos.x < this.position.x+this.size.x && v_pos.y < this.position.y+this.size.y && v_pos.z < this.position.z+this.size.z) {
                // Check if any of the children leaves have the point //
                if(this.leaves.length > 0) {
                    for(var leaf of this.leaves) {
                        let l = leaf.find_leaf(v_pos);
                        if(l) {
                            //nix.io.putf("Found leaf at (%,%,%)\n", l.position.x, l.position.y, l.position.z);
                            return l;
                        }
                    }
                } else {
                    return this;
                }
            }

            return null;
        }

        generate_mesh(vertices, indices, uvs, normals) {
            /*
             * Generate a mesh by running through the Octree and asking for one
             */

            if(!this.leaves.length && this.solid) {
                let ind = 0;
                if(!this.neighbors.py || !this.neighbors.py.solid) {
                    ind = vertices.length/3;
                    Array.prototype.push.apply(vertices, [this.position.x              , this.position.y + this.size.y, this.position.z,
                                                          this.position.x + this.size.x, this.position.y + this.size.y, this.position.z,
                                                          this.position.x + this.size.x, this.position.y + this.size.y, this.position.z + this.size.z,
                                                          this.position.x              , this.position.y + this.size.y, this.position.z + this.size.z]);
                    Array.prototype.push.apply(uvs, [this.position.x              , this.position.z,
                                                     this.position.x + this.size.x, this.position.z,
                                                     this.position.x + this.size.x, this.position.z + this.size.z,
                                                     this.position.x              , this.position.z + this.size.z]);
                    Array.prototype.push.apply(normals, [0.0, 1.0, 0.0,
                                                         0.0, 1.0, 0.0,
                                                         0.0, 1.0, 0.0,
                                                         0.0, 1.0, 0.0]);
                    Array.prototype.push.apply(indices, [0+ind, 2+ind, 1+ind,   2+ind, 0+ind, 3+ind]);
                }

                if(!this.neighbors.ny || !this.neighbors.ny.solid) {
                    ind = vertices.length/3;
                    Array.prototype.push.apply(vertices, [this.position.x              , this.position.y, this.position.z,
                                                          this.position.x + this.size.x, this.position.y, this.position.z,
                                                          this.position.x + this.size.x, this.position.y, this.position.z + this.size.z,
                                                          this.position.x              , this.position.y, this.position.z + this.size.z]);
                    Array.prototype.push.apply(uvs, [this.position.x              , this.position.z,
                                                     this.position.x + this.size.x, this.position.z,
                                                     this.position.x + this.size.x, this.position.z + this.size.z,
                                                     this.position.x              , this.position.z + this.size.z]);
                    Array.prototype.push.apply(normals, [0.0, -1.0, 0.0,
                                                        0.0, -1.0, 0.0,
                                                        0.0, -1.0, 0.0,
                                                        0.0, -1.0, 0.0]);
                    Array.prototype.push.apply(indices, [0+ind, 1+ind, 2+ind,   2+ind, 3+ind, 0+ind]);
                }

                if(!this.neighbors.nz || !this.neighbors.nz.solid) {
                    ind = vertices.length/3;
                    Array.prototype.push.apply(vertices, [this.position.x              , this.position.y              , this.position.z,
                                                          this.position.x + this.size.x, this.position.y              , this.position.z,
                                                          this.position.x + this.size.x, this.position.y + this.size.y, this.position.z,
                                                          this.position.x              , this.position.y + this.size.y, this.position.z]);
                    Array.prototype.push.apply(uvs, [this.position.x              , this.position.y              ,
                                                     this.position.x + this.size.x, this.position.y              ,
                                                     this.position.x + this.size.x, this.position.y + this.size.y,
                                                     this.position.x              , this.position.y + this.size.y,]);
                    Array.prototype.push.apply(normals, [0.0, 0.0, -1.0,
                                                        0.0, 0.0, -1.0,
                                                        0.0, 0.0, -1.0,
                                                        0.0, 0.0, -1.0]);
                    Array.prototype.push.apply(indices, [0+ind, 2+ind, 1+ind,   2+ind, 0+ind, 3+ind]);
                }

                if(!this.neighbors.pz || !this.neighbors.pz.solid) {
                    ind = vertices.length/3;
                    Array.prototype.push.apply(vertices, [this.position.x              , this.position.y              , this.position.z + this.size.z,
                                                          this.position.x + this.size.x, this.position.y              , this.position.z + this.size.z,
                                                          this.position.x + this.size.x, this.position.y + this.size.y, this.position.z + this.size.z,
                                                          this.position.x              , this.position.y + this.size.y, this.position.z + this.size.z]);
                    Array.prototype.push.apply(uvs, [this.position.x              , this.position.y              ,
                                                     this.position.x + this.size.x, this.position.y              ,
                                                     this.position.x + this.size.x, this.position.y + this.size.y,
                                                     this.position.x              , this.position.y + this.size.y,]);
                    Array.prototype.push.apply(normals, [0.0, 0.0, 1.0,
                                                        0.0, 0.0, 1.0,
                                                        0.0, 0.0, 1.0,
                                                        0.0, 0.0, 1.0]);
                    Array.prototype.push.apply(indices, [0+ind, 1+ind, 2+ind,   2+ind, 3+ind, 0+ind]);
                }

                if(!this.neighbors.nx || !this.neighbors.nx.solid) {
                    ind = vertices.length/3;
                    Array.prototype.push.apply(vertices, [this.position.x, this.position.y              , this.position.z,
                                                          this.position.x, this.position.y              , this.position.z + this.size.z,
                                                          this.position.x, this.position.y + this.size.y, this.position.z + this.size.z,
                                                          this.position.x, this.position.y + this.size.y, this.position.z]);
                    Array.prototype.push.apply(uvs, [this.position.y              , this.position.z,
                                                     this.position.y              , this.position.z + this.size.z,
                                                     this.position.y + this.size.y, this.position.z + this.size.z,
                                                     this.position.y + this.size.y, this.position.z]);
                    Array.prototype.push.apply(normals, [-1.0, 0.0, 0.0,
                                                        -1.0, 0.0, 0.0,
                                                        -1.0, 0.0, 0.0,
                                                        -1.0, 0.0, 0.0]);
                    Array.prototype.push.apply(indices, [0+ind, 1+ind, 2+ind,   2+ind, 3+ind, 0+ind]);
                }

                if(!this.neighbors.px || !this.neighbors.px.solid) {
                    ind = vertices.length/3;
                    Array.prototype.push.apply(vertices, [this.position.x + this.size.x, this.position.y              , this.position.z,
                                                          this.position.x + this.size.x, this.position.y              , this.position.z + this.size.z,
                                                          this.position.x + this.size.x, this.position.y + this.size.y, this.position.z + this.size.z,
                                                          this.position.x + this.size.x, this.position.y + this.size.y, this.position.z]);
                    Array.prototype.push.apply(uvs, [this.position.y              , this.position.z,
                                                     this.position.y              , this.position.z + this.size.z,
                                                     this.position.y + this.size.y, this.position.z + this.size.z,
                                                     this.position.y + this.size.y, this.position.z]);
                    Array.prototype.push.apply(normals, [1.0, 0.0, 0.0,
                                                        1.0, 0.0, 0.0,
                                                        1.0, 0.0, 0.0,
                                                        1.0, 0.0, 0.0]);
                    Array.prototype.push.apply(indices, [0+ind, 2+ind, 1+ind,   2+ind, 0+ind, 3+ind]);
                }
            } else {
                for(let leaf of this.leaves) {
                    leaf.generate_mesh(vertices, indices, uvs, normals);
                }                
            }
        }
    }

    return SUCCESS;
});