new modulator.Module('voxel', 'nix', (function(){
    // Gio is cool I guess //

    ::voxel = { };

    nix::registry.set('/nix/voxel', this.voxel);

    modulator.load("block.js", true,
                   "chunk.js", true,
                   "world.js", true,
                   "octree.js", true,
                   "generator.js", true);

    return SUCCESS;
}));