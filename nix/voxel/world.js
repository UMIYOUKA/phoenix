new modulator.Module('nix_voxel_world', 'nix', (function(){
    /*globals nix */
    ::voxel::world = class _internal_voxel_world {
        constructor(gl, generator, offset, dimensions, chunk_size, vname) {
            this.name = vname;

            this.chunk_faces = {
                top     : [0.0,1.0,0.0, 1.0,1.0,0.0, 1.0,1.0,1.0, 0.0,1.0,1.0],
                bottom  : [0.0,0.0,0.0, 1.0,0.0,0.0, 1.0,0.0,1.0, 0.0,0.0,1.0],

                left    : [0.0,0.0,0.0, 0.0,0.0,1.0, 0.0,1.0,1.0, 0.0,1.0,0.0],
                right   : [1.0,0.0,0.0, 1.0,0.0,1.0, 1.0,1.0,1.0, 1.0,1.0,0.0],

                front   : [0.0,0.0,1.0, 1.0,0.0,1.0, 1.0,1.0,1.0, 0.0,1.0,1.0],
                back    : [0.0,0.0,0.0, 1.0,0.0,0.0, 1.0,1.0,0.0, 0.0,1.0,0.0],

                indices : {
                    normal  : [0, 1, 2, 2, 3, 0],
                    inverse : [2, 1, 0, 0, 3, 2]
                },

                normals : {
                    top    : [0.0, 1.0,0.0,  0.0, 1.0,0.0,  0.0, 1.0,0.0,  0.0, 1.0,0.0],
                    bottom : [0.0,-1.0,0.0,  0.0,-1.0,0.0,  0.0,-1.0,0.0,  0.0,-1.0,0.0],

                    left   : [-1.0,0.0,0.0, -1.0,0.0,0.0, -1.0,0.0,0.0, -1.0,0.0,0.0],
                    right  : [ 1.0,0.0,0.0,  1.0,0.0,0.0,  1.0,0.0,0.0,  1.0,0.0,0.0],

                    front  : [0.0,0.0, 1.0,  0.0,0.0, 1.0,  0.0,0.0, 1.0,  0.0,0.0, 1.0],
                    back   : [0.0,0.0,-1.0,  0.0,0.0,-1.0,  0.0,0.0,-1.0,  0.0,0.0,-1.0]
                },

                uvs     : [0.0,0.0, 1.0,0.0, 1.0,1.0, 0.0,1.0]
            };

            this.gl = gl;
            this.generator = generator;

            this.chunks = new Array(dimensions.width  *
                                    dimensions.height *
                                    dimensions.length);

            this.offset     = offset;
            this.dimensions = dimensions;
            this.chunk_size = chunk_size;

            this.blocks = nix::registry.get('/nix/voxel/blocks').value;

            let block_current = 0;
            this.atlas = new nix::gl::Atlas(this.gl, 32, 32, 8, this.blocks.length + 1, 1);

            let load_block = function() {
                if(block_current === this.blocks.length) return;
                block_current += 1;
                this.blocks[block_current - 1].atlas_pos = this.atlas.load_tile(this.blocks[block_current - 1].texture_name, load_block);
            }.bind(this);

            load_block();
        }

        get_chunk(x, y, z) {
            if(x > this.dimensions.width  - 1 ||
            y > this.dimensions.height - 1 ||
            z > this.dimensions.length - 1 ||
            x < 0 || y < 0 || z < 0) return null;
            return this.chunks[x+(z*this.dimensions.width)+(y*this.dimensions.width*this.dimensions.length)];
        }

        pre_generate_chunks() {
            for(let j=0; j<this.dimensions.height; ++j) {
                for(let i=0; i<this.dimensions.width; ++i) {
                    for(let k=0; k<this.dimensions.length; ++k) {
                        let pos = i+(k*this.dimensions.width)+(j*this.dimensions.width*this.dimensions.length);
                        if(!this.chunks[pos]) {
                            // nix.io.putf('[nix::voxel::world::step_generate_chunks] [%] Generating chunk at {x:% y:% z:%}\n', this.name, i, j, k);
                            let chunk = new nix.voxel.Chunk(this,
                                                            {x:(i - this.offset.x)*this.chunk_size.width,
                                                            y:(j - this.offset.y)*this.chunk_size.height,
                                                            z:(k - this.offset.z)*this.chunk_size.length});
                            chunk.generate();
                            this.chunks[pos] = chunk;
                        }
                    }
                }

                nix::io.putf('[nix::voxel::world::pre_generate_chunks] [%] Generating chunks... %%\n', this.name, (100 / this.dimensions.height) * (j + 1), '%');
            }

            for(let j=0; j<this.dimensions.height; ++j) {
                for(let i=0; i<this.dimensions.width; ++i) {
                    for(let k=0; k<this.dimensions.length; ++k) {
                        let c = this.get_chunk(i,j,k);

                        c.neighbors = {
                            px : this.get_chunk(i+1,j,k),
                            nx : this.get_chunk(i-1,j,k),

                            py : this.get_chunk(i,j+1,k),
                            ny : this.get_chunk(i,j-1,k),

                            pz : this.get_chunk(i,j,k+1),
                            nz : this.get_chunk(i,j,k-1)
                        };

                        /*if(!c.neighbors.px) c.neighbors.px = {pos:{x:0,y:0,z:0}};
                        if(!c.neighbors.nx) c.neighbors.nx = {pos:{x:0,y:0,z:0}};
                        if(!c.neighbors.py) c.neighbors.py = {pos:{x:0,y:0,z:0}};
                        if(!c.neighbors.ny) c.neighbors.ny = {pos:{x:0,y:0,z:0}};
                        if(!c.neighbors.pz) c.neighbors.pz = {pos:{x:0,y:0,z:0}};
                        if(!c.neighbors.nz) c.neighbors.nz = {pos:{x:0,y:0,z:0}};


                        nix.io.putf('neighbor info:\nor(%,%,%)\npx(%,%,%) nx(%,%,%)\npy(%,%,%) ny(%,%,%)\npz(%,%,%) nz(%,%,%)\n',
                                    c.pos.x, c.pos.y, c.pos.z,
                                    c.neighbors.px.pos.x, c.neighbors.px.pos.y, c.neighbors.px.pos.z,
                                    c.neighbors.nx.pos.x, c.neighbors.nx.pos.y, c.neighbors.nx.pos.z,
                                    c.neighbors.py.pos.x, c.neighbors.py.pos.y, c.neighbors.py.pos.z,
                                    c.neighbors.ny.pos.x, c.neighbors.ny.pos.y, c.neighbors.ny.pos.z,
                                    c.neighbors.pz.pos.x, c.neighbors.pz.pos.y, c.neighbors.pz.pos.z,
                                    c.neighbors.nz.pos.x, c.neighbors.nz.pos.y, c.neighbors.nz.pos.z);*/
                    }
                }

                nix::io.putf('[nix::voxel::world::pre_generate_chunks] [%] Identifying neighbors... %%\n', this.name, (100 / this.dimensions.height) * (j + 1), '%');
            }

        }

        step_generate_meshes(blocksize) {
            let gen_counter = blocksize;

            for(let chunk of this.chunks) {
                if(!chunk.mesh && !chunk.mesh_worthless) {
                    chunk.create_mesh();

                    gen_counter--;

                    if(gen_counter <= 0) return;
                }
            }
        }

        step_generate_chunks() {
            let gen_counter = 5;

            for(let j=0; j<this.dimensions.height; ++j) {
                for(let i=0; i<this.dimensions.width; ++i) {
                    for(let k=0; k<this.dimensions.length; ++k) {
                        let pos = i+(k*this.dimensions.width)+(j*this.dimensions.width*this.dimensions.length);
                        if(!this.chunks[pos]) {
                            // nix.io.putf('[nix::voxel::world::step_generate_chunks] [%] Generating chunk at {x:% y:% z:%}\n', this.name, i, j, k);
                            let chunk = new nix.voxel.Chunk(this,
                                                            {x:i*this.chunk_size.width,
                                                            y:j*this.chunk_size.height,
                                                            z:k*this.chunk_size.length});
                            chunk.generate();
                            chunk.create_mesh();
                            this.chunks[pos] = chunk;

                            gen_counter--;
                            if(gen_counter <= 0) {
                                return;
                            }
                        }
                    }
                }
            }
        }

        render(matrix_callback) {
            for(let chunk of this.chunks) {
                if(!chunk || !chunk.mesh) continue;

                matrix_callback(chunk);
                chunk.mesh.bind();
                chunk.mesh.draw();
            }
        }
    };

    return SUCCESS;
}));
