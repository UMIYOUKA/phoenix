new modulator.Module('nix_voxel_chunk', 'nix', (function() {
    nix::voxel::Chunk = class _internal_voxel_chunk {
        constructor(world, pos) {
            this.world = world;

            this.width  = this.world.chunk_size.width;
            this.height = this.world.chunk_size.height;
            this.length = this.world.chunk_size.length;

            this.pos = pos;

            this.data = new Array(this.width  *
                                this.height *
                                this.length);

            this.mesh_data = {
                vertices  : [],
                indices   : [],
                normals   : [],
                uvs       : [],
            };

            this.neighbors = {
                px : null,
                nx : null,

                py : null,
                ny : null,

                pz : null,
                nz : null
            };

            this.matrix = new nix.gl.mat4();
            this.matrix.translate(vec3.fromValues(this.pos.x, this.pos.y, this.pos.z));
        }

        generate() {
            this.world.generator.generate(this.data, this.pos, {width:this.width, height:this.height, length:this.length});
        }

        create_mesh() {
            this.mesh_data.vertices.length = 0;
            this.mesh_data.indices.length  = 0;
            this.mesh_data.uvs.length      = 0;
            this.mesh_data.normals.length  = 0;

            if(this.mesh) {
                this.mesh.destroy();
            }

            let v_length = 0;

            let r_data = 0;
            let l_data = 0;

            let f_data = 0;
            let b_data = 0;

            let u_data = 0;
            let d_data = 0;

            for(let w = 0; w < this.width; ++w) {
                for(let l = 0; l < this.length; ++l) {
                    for(let h = 0; h < this.height; ++h) {
                        let pos = w+(l*this.width)+(h*this.width*this.length);

                        if(this.data[pos]) {
                            let uvs = this.world.chunk_faces.uvs.map(function(x, i) {
                                return !(i % 2 == 0) ? (x * this.world.atlas.tile.size_h) + this.world.atlas.tile.border_h_nudge : (x * this.world.atlas.tile.size_w) + this.world.atlas.tile.border_w_nudge + (this.world.atlas.tile.rect_w * this.data[pos]);
                            }.bind(this));

                            if(h > 0 && h < this.height - 1) {
                                u_data = this.data[pos + (this.width*this.length)];
                                d_data = this.data[pos - (this.width*this.length)];
                            } else {
                                u_data = 0;
                                d_data = 0;

                                if(h > 0) {
                                    d_data = this.data[pos - (this.width*this.length)];

                                    if(this.neighbors.py) {
                                        u_data = this.neighbors.py.data[w + (l*this.width)];
                                    }
                                } else if(h < this.height - 1) {
                                    u_data = this.data[pos + (this.width*this.length)];

                                    if(this.neighbors.ny) {
                                        d_data = this.neighbors.ny.data[w + (l*this.width) + ((this.height - 1) * this.width * this.length)];
                                    }
                                }
                            }

                            if(w > 0 && w < this.width - 1) {
                                r_data = this.data[pos + 1];
                                l_data = this.data[pos - 1];
                            } else {
                                r_data = 0;
                                l_data = 0;

                                if(w > 0) {
                                    l_data = this.data[pos - 1];

                                    if(this.neighbors.px) {
                                        r_data = this.neighbors.px.data[(l*this.width)+(h*this.width*this.length)];
                                    }
                                } else if(w < this.width - 1) {
                                    r_data = this.data[pos + 1];

                                    if(this.neighbors.nx) {
                                        l_data = this.neighbors.nx.data[(this.width - 1) + (l*this.width) + (h*this.width*this.length)];
                                    }
                                }
                            }

                            if(l > 0 && l < this.length - 1) {
                                f_data = this.data[pos + this.width];
                                b_data = this.data[pos - this.width];
                            } else {
                                f_data = 0;
                                b_data = 0;

                                if(l > 0) {
                                    b_data = this.data[pos - this.width];

                                    if(this.neighbors.pz) {
                                        f_data = this.neighbors.pz.data[w + (h*this.width*this.length)];
                                    }
                                } else if(l < this.length - 1) {
                                    f_data = this.data[pos + this.width];

                                    if(this.neighbors.nz) {
                                        b_data = this.neighbors.nz.data[w + ((this.length - 1) * this.width) + (h*this.width*this.length)];
                                    }
                                }
                            }


                            if(!u_data) {
                                Array.prototype.push.apply(this.mesh_data.indices , this.world.chunk_faces.indices.inverse.map(function(x) { return x + v_length; }));
                                Array.prototype.push.apply(this.mesh_data.vertices, this.world.chunk_faces.top.map(function(x, i) { return (i%3==0) ? x+w : ((i-1)%3==0) ? x+h : x+l; }));
                                Array.prototype.push.apply(this.mesh_data.uvs     , uvs);
                                Array.prototype.push.apply(this.mesh_data.normals , this.world.chunk_faces.normals.top);
                                v_length += 4;
                            }

                            if(!d_data) {
                                Array.prototype.push.apply(this.mesh_data.indices , this.world.chunk_faces.indices.normal.map(function(x) { return x + v_length; }));
                                Array.prototype.push.apply(this.mesh_data.vertices, this.world.chunk_faces.bottom.map(function(x, i) { return (i%3==0) ? x+w : ((i-1)%3==0) ? x+h : x+l; }));
                                Array.prototype.push.apply(this.mesh_data.uvs     , uvs);
                                Array.prototype.push.apply(this.mesh_data.normals , this.world.chunk_faces.normals.bottom);
                                v_length += 4;
                            }

                            if(!l_data) {
                                Array.prototype.push.apply(this.mesh_data.indices , this.world.chunk_faces.indices.normal.map(function(x) { return x + v_length; }));
                                Array.prototype.push.apply(this.mesh_data.vertices, this.world.chunk_faces.left.map(function(x, i) { return (i%3==0) ? x+w : ((i-1)%3==0) ? x+h : x+l; }));
                                Array.prototype.push.apply(this.mesh_data.uvs     , uvs);
                                Array.prototype.push.apply(this.mesh_data.normals , this.world.chunk_faces.normals.left);
                                v_length += 4;
                            }

                            if(!r_data) {
                                Array.prototype.push.apply(this.mesh_data.indices , this.world.chunk_faces.indices.inverse.map(function(x) { return x + v_length; }));
                                Array.prototype.push.apply(this.mesh_data.vertices, this.world.chunk_faces.right.map(function(x, i) { return (i%3==0) ? x+w : ((i-1)%3==0) ? x+h : x+l; }));
                                Array.prototype.push.apply(this.mesh_data.uvs     , uvs);
                                Array.prototype.push.apply(this.mesh_data.normals , this.world.chunk_faces.normals.right);
                                v_length += 4;
                            }

                            if(!b_data) {
                                Array.prototype.push.apply(this.mesh_data.indices , this.world.chunk_faces.indices.inverse.map(function(x) { return x + v_length; }));
                                Array.prototype.push.apply(this.mesh_data.vertices, this.world.chunk_faces.back.map(function(x, i) { return (i%3==0) ? x+w : ((i-1)%3==0) ? x+h : x+l; }));
                                Array.prototype.push.apply(this.mesh_data.uvs     , uvs);
                                Array.prototype.push.apply(this.mesh_data.normals , this.world.chunk_faces.normals.back);
                                v_length += 4;
                            }

                            if(!f_data) {
                                Array.prototype.push.apply(this.mesh_data.indices , this.world.chunk_faces.indices.normal.map(function(x) { return x + v_length; }));
                                Array.prototype.push.apply(this.mesh_data.vertices, this.world.chunk_faces.front.map(function(x, i) { return (i%3==0) ? x+w : ((i-1)%3==0) ? x+h : x+l; }));
                                Array.prototype.push.apply(this.mesh_data.uvs     , uvs);
                                Array.prototype.push.apply(this.mesh_data.normals , this.world.chunk_faces.normals.front);
                                v_length += 4;
                            }
                        }
                    }
                }
            }

            if(!this.mesh_data.vertices.length) {
                this.mesh_worthless = true;
                return;
            }

            this.mesh_worthless = false;

            if(!this.mesh) {
                this.mesh = new nix.gl.static_mesh(this.world.gl, this.mesh_data.vertices, this.mesh_data.uvs, this.mesh_data.indices, this.mesh_data.normals);
            } else {
                this.mesh.create(this.mesh_data.vertices, this.mesh_data.uvs, this.mesh_data.indices, this.mesh_data.normals);
            }
        }
    };

    return SUCCESS;
}));