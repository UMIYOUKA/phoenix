new modulator.Module('nix_voxel_block', 'nix', (function(this_space){
    ::voxel::Block = class _internal_voxel_Block {
        constructor() {
            this.name         = "nix_voxel_block_generic";
            this.display_name = "Generic Block";
            this.description  = "Gio thinks this is very generic, isn't it?";

            this.model   = null;
            this.texture_name = "resources/textures/basic_block.png";

            this.atlas_pos;

            this.uv = {
                right  : null,
                top    : null,
                front  : null,

                left   : null,
                bottom : null,
                back   : null
            };

            this.properties = {
                visibility : [1, 1, 1, 1, 1, 1],
                collision  : true,

                hardness         : 0,
                mining_time      : 1.0,
                mining_particles : "none",

                tool : "nix::none"
            };
        }

        from_block_object(block) {
            this.name         = block.name;
            this.display_name = block.display_name;
            this.description  = block.description;

            this.model        = block.model;
            this.texture_name = block.texture;

            this.uv = block.uv;

            this.properties = block.properties;
        }
    };

    nix::registry.set('/nix/voxel/blocks', []);

    nix::registry.set('/nix/resources/handlers/block',
        new nix::ResourceHandler("block",
            function(resource, loader) {
                let block_raw = JSON.parse(resource.data);
                let block = new nix.voxel.Block();
                block.from_block_object(block_raw);
                block.texture_name = resource.path + block.texture_name;

                if(nix::registry.exists('/nix/voxel/blocks/' + block.name)) {
                    nix::io.putf('[resource_handler::block] Error! Block name clash detected for block "%"', block.name);
                    return false;
                }

                nix::registry.get('/nix/voxel/blocks').value.push(block);
                nix::registry.set('/nix/voxel/blocks/' + block.name, block);
                return true;
            })
    );

    return SUCCESS;
}));