new modulator.Module('default_handlers', 'nix', (function() {
    nix::registry.set('/nix/resources/handlers');
    nix::registry.set('/nix/resources/handlers/txt',
        new nix::ResourceHandler("txt",
            function(resource) {
                nix.io.put(resource.data + "\n");
                return true;
            })
    );

    nix::registry.set('/nix/resources/handlers/list',
        new nix::ResourceHandler("list",
            function(resource, loader) {
                // nix.io.put("[!!!STUB!!!] " + resource.data + "\n");
                let list = JSON.parse(resource.data);
                for(let data of list.data) {
                    loader.load(resource.rootless_path + data.path);
                }
                return true;
            })
    );

    return SUCCESS;
}));
