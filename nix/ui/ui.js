new modulator.Module('ui', 'nix', (function() {
    nix.set_global_list("NIX_WIDGET_NONE", 0,
                        "NIX_WIDGET_LAYOUT_VERTICAL",   10,
                        "NIX_WIDGET_LAYOUT_HORIZONTAL", 11,
                        "NIX_WIDGET_POSITION_START",  -1,
                        "NIX_WIDGET_POSITION_END",    -2,
                        "NIX_WIDGET_POSITION_CENTER", -3,
                        "NIX_WIDGET_SIZE_EXPAND", -1,
                        "NIX_WIDGET_SIZE_SHRINK", -2);
    
    ::Widget = class __nix_internal_ui_Widget {
        constructor() {
            this.type   = NIX_WDGET_NONE;

            this.layout = {
                width  : NIX_WIDGET_SIZE_EXPAND,
                height : NIX_WIDGET_SIZE_EXPAND,

                horizontal : NIX_WIDGET_POSITION_START,
                vertical   : NIX_WIDGET_POSITION_START
            };
        }

        from_obj() {

        }
    };

    ::Interface = class __nix_internal_ui_Interface {

    };

    return SUCCESS;
}));
