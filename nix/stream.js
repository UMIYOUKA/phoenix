new modulator.Module('stream', 'nix', (function() {
    /*
     * Stream
     *
     * Takes in data and passes it on to listeners
     * 
     */

    ::Stream = class _internal_stream_Stream {
        constructor() {
            this.listeners  = [];
        }

        add_listener(af_listener) {
            this.listeners.push(af_listener);
            return this.listeners.size;
        }

        put() {
            for(let o_msg of arguments) {
                this.current_object = o_msg;

                for(let f_l of this.listeners) {
                    f_l(o_msg);
                }
            }
        }
    };

    /* 
     * TextStream
     *
     * A helper class that adds formatting functions
     * - put_line(s_message) :: Does exactly what it says
     * - putf(s_message, ...) :: Formats a string and prints it
     * | % - Simple string substitution
     * | ^ n% - Pads left N characters
     * | $ Replaces a string with something in the registry
     * 
     */

    ::TextStream = class _internal_stream_TextStream extends ::Stream {
        constructor() {
            super();
        }

        put_line(s_message) {
            this.put(s_message + "\n");
        }

        putf(s_message) {
            let i_replacement = 0;
            let s_out = "";

            let segments = s_message.split(/(%)|(\$[^\$]+\$)|(\^.[0-9]+)/);

            let p_char = ' ';
            let pad    = null;

            for(let seg of segments) {
                if(!seg)
                    continue;

                switch(seg[0]) {
                    case '%':
                        i_replacement++;
                        if(!pad) {
                            s_out += arguments[i_replacement];
                            continue;
                        }

                        let padding = Array(pad).join(p_char);
                        s_out += (arguments[i_replacement] + padding).substring(0, padding.length);

                        pad = null;
                        continue;
                    case '$':
                        s_out += (nix.registry.get(seg.substr(1, seg.length - 2)) || {value:"[KEY NOT FOUND]"}).value;
                        continue;
                    case '^':
                        p_char = seg[1];
                        pad    = parseInt(seg.substr(2, seg.length));
                        continue;
                }

                s_out += seg;
            }

            this.put(s_out);
        }
    };

    return SUCCESS;
}));

