new modulator.Module('math', 'nix', (function() {
    Math.powerOfTwo = function(x) {
        return (Math.log2(x) % 1) === 0;
    };

    Number.prototype.clamp = function(min, max) {
        return Math.min(Math.max(this, min), max);
    };

    Math.vecAdd = function(v1, v2) {
        return {x:v1.x+v2.x, y:v1.y+v2.y, z:v1.z+v2.z};
    };

    return SUCCESS;
}));