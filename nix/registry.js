new modulator.Module('registry', 'nix', (function(this_module){
    /*
    *  So here's the registry...
    *  it's really weirdly complicated buy I'll try to explain
    *  it my best
    *
    *  [ nix.key / _phoenix_registry_key ]
    *  A class that contains all of the functions needed by
    *  the registry
    *
    *  [ key.search ]
    *  A recursive function that goes throught each of the
    *  children trying to figure out what key has what name
    *
    *  [ key.search_path ]
    *  uses the result from key.follow_path to decide whether
    *  or not a key exists
    *
    *  [ key.follow_path ]
    *  now, this is the janky - what is going on - type of
    *  function that makes me question my sanity. and yes,
    *  it's very recursive.
    *
    *  firstly,
    *  we split the path by '/', set up our basic variables,
    *  and then sort out empty statements like '//' by testing
    *  for blank strings. the 'current key' is then set 'this'
    *
    *  then we start our loop,
    *
    *  we get our current path segment and store it in 'p',
    *  then we loop through all of the currently selected
    *  keys' keys. it will check if the key exists in the
    *  current path node, if not, break the loop and give
    *  an error / 'null' back.
    *
    *  if it does find it, set the current key to the found one
    *  and repeat what happened before until the end is reached
    *
    *  at the end it returns an object with information needed
    *  for the decyphering of what just happened
    *
    * [ key.get ]
    * Uses the result from key.follow_path and if the key exists
    * it returns it, if not, gives an error
    *
    * [ key.set ]
    * Uses the result from key.follow_path to make sure a new
    * key is added to the right place at the end of the path,
    * if any segment is broken before the end it will return
    * an error
    *
    * [ key.add ]
    * adds a new key to the current object
    *
    * [ key.branch ]
    * A completely crazy looking recursive function that returns
    * a fairly nice looking tree to the caller in the form of a
    * string. It was a really strange function to make so I'm
    * not even going to try to explain what it does just yet.
    * But now that I look at it, it doesn't look that crazy, it
    * just has a crazy logic behind how it works
    * 
    */

    ::Key = class _internal_registry_Key {
        constructor(s_name, o_value) {
            this.name  = s_name;
            this.value = (!o_value) ? 0 : o_value;
            this.keys  = [];
        }

        search(s_name) {
            if(s_name.charAt('/') > -1) {
                return this.search_path(s_name);
            }

            for(let k of this.keys) {
                if(k.name != s_name) {
                    if(k.search(s_name)) {
                        return s_name;
                    }
                }
            }

            return null;
        };

        search_path(s_path) {
            let kp = this.follow_path(s_path);

            if(kp.key.name != kp.last) {
                nix::io.putf("[nix::registry::key::search_path] Failed to find key '%' in path '%'", kp.last, s_path);
                return null;
            }

            return kp.key;
        }

        follow_path(s_path) {
            let paths     = s_path.split('/');
            let k_current = this;
            let k_next    = paths[1];
            let no_loop   = false;

            paths = paths.filter(Boolean); // Sort out empty strings //

            for(let i = 0; i < paths.length; ++i) {
                let p = paths[i];

                if(!k_current.keys.length) {
                    break;
                }

                for(let k of k_current.keys) {
                    if(no_loop) {
                        i = paths.length;
                        break;
                    }

                    if(k.name == p) {
                        k_current = k;
                        break;
                    }

                    if(k.name == k_current.keys[k_current.keys.length - 1].name) {
                        i = paths.length;
                        no_loop = true;
                        break;
                    }
                }

                if(paths[i + 1]) {
                    k_next = paths[i + 1];
                } else {
                    k_next = p;
                }
            }

            // nix::io.putf('result : ck : % : kn : % : kl : %\n', k_current.name, k_next, paths[paths.length - 1]);

            return { key     : k_current,
                     next    : k_next,
                     last    : paths[paths.length - 1] };
        }

        get(s_path, b_quiet) {
            let k_pathinfo = this.follow_path(s_path);

            if(k_pathinfo.key.name == k_pathinfo.last) {
                return k_pathinfo.key;
            }

            if(!b_quiet) {
                nix::io.putf("[nix::registry::key::get] Cannot get key '%', broken path '%' at key '%'\n", k_pathinfo.last, s_path, k_pathinfo.next);
            }

            return null;
        }

        set(s_path, o_value, b_quiet, b_recurse) {
            let k_pathinfo = this.follow_path(s_path);

            if(k_pathinfo.next == k_pathinfo.last) {
                return k_pathinfo.key.add(k_pathinfo.next, o_value);
            }

            if(!b_quiet) {
                nix::io.putf("[nix::registry::key::set] Cannot set key '%', broken path '%' at key '%'\n", k_pathinfo.last, s_path, k_pathinfo.next);
            }

            if(!b_recurse)
                return null;

            k_pathinfo.b_recurse = true;
            return k_pathinfo;
        }

        set_full_path(s_path, o_value) {
            let k_pathinfo = this.set(s_path, o_value, true, true);

            if(!k_pathinfo.b_recurse) {
                return k_pathinfo; // Key is set
            }

            k_pathinfo.key.add(k_pathinfo.next);
            return this.set_full_path(s_path, o_value);
        }

        exists(s_path) {
            return (this.get(s_path, true) !== null);
        }

        add(s_name, o_value) {
            let k = this.search(s_name);
            // nix.io.putf('[nix::registry::key::set] Setting key "%"\n', name);

            if(k) {
                k.value = o_value;
                return k;
            }

            k = new nix.Key(s_name, o_value);
            k.parent = this;

            this.keys.push(k);

            return k;
        }

        path(k_node) {
            let p = ["/"];
            p.push(this.name);

            if(this.parent) {
                p = p.concat(this.parent.path(true));
            }

            if(k_node) {
                return p;
            }

            p.push('/');
            return p.reverse().join('');
        }

        branch(i_depth, i_last, branches, i_dst) {
            let d = (i_depth !== undefined && i_depth !== null) ? i_depth : 0;

            let out = "";

            if(!branches) {
                branches = [];
            }

            branches.push(true);

            for(let i = 0; i < d - 1; ++i) {
                if(branches[i]) {
                    out += "  ";
                } else {
                    out += "│ ";
                }
            }

            if(d) {
                if(i_last) {
                    out += "└";
                } else {
                    out += "├";
                }

                if(this.keys.length) {
                    out += "─┬╼ ";
                } else {
                    out += "──╼ ";
                }
            } else {
                out += "┌╼ ";
            }

            if(i_dst) {
                let pad = Array(i_dst).join(' ');
                out += (this.name + pad).substr(0, i_dst) + " : " + this.value + "\n";
            } else {
                out += this.name + " : " + this.value + "\n";
            }

            let k_dst = 0;

            if(this.keys.length) {
                for(let k of this.keys) {
                    if(k.name.length > k_dst) {
                        k_dst = k.name.length;
                    }
                }
            }

            for(let i = 0; i < this.keys.length; ++i) {
                branches[d] = (i == this.keys.length - 1);
                out += this.keys[i].branch(d + 1, (i == this.keys.length - 1), branches, k_dst);
            }

            branches.pop();

            return out;
        }
    };

    ::Registry = class _internal_registry_Registry {
        constructor() {
            this.root = new nix::Key("root", null);
        }

        get(s_path) {
            return this.root.get(s_path);
        }

        set(s_path, o_value) {
            for(let i = 0; i < arguments.length; i += 2) {
                this.root.set(arguments[i], arguments[i + 1]);
            }

            return SUCCESS;
        }

        set_not_exists(s_path, o_value) {
            if(!this.exists(s_path)) {
                return this.set(s_path, o_value);
            }

            return SUCCESS; // No real fail can happen here //
        }

        set_full_path(s_path, o_value) {
            return this.root.set_full_path(s_path, o_value);
        }

        exists(s_path) {
            return this.root.exists(s_path);
        }
    };

    return SUCCESS;
}));