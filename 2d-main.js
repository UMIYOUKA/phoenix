new modulator.Module('main', 'main', (function() {
    let gl       = null;
    let s_screen = null;

    let rl_loader = null;

    ::main = function() {
        nix::io.putf('Starting this 2D game or something\n');

        // Get the screen and the OpenGL context //

        s_screen = document.getElementById('screen');
        gl       = s_screen.context;

        s_screen.resize_to_client();

        // Create a resource loader and make it use our GL so //
        // graphical assets can load                          //

        rl_loader = new nix::ResourceLoader('2d-resources/');
        rl_loader.use_gl(gl);

        // Set some game variables in the registry so other //
        // functions can access them P R O P E R L Y        //

        nix::registry.set('/player', {},
                          '/player/0', {
                              name   : "LIZ",
                              level  : 1,
                              items  : [],
                              skills : [],
                              stats  : {
                                  health : 10,
                                  mana   : 10,
 
                                  agility  : 5,
                                  strength : 5,
                                  speed    : 5
                              }
                          });
 
        // Open the main menu //
        main::main_menu();
    };

    ::main_menu = function() {
        nix::gl::clear_with_color(gl, 0, 0, 0, 1);

        let p_sprite = rl_loader.load('shaders/sprite.prog');
        p_sprite.use();

        let ul_sprite_texpos = p_sprite.uniform_location('tex');
        let ul_sprite_mvppos = p_sprite.uniform_location('mvp');

        let tex_atlas = rl_loader.load('textures/menu.atlas');
        gl.uniform1i(ul_sprite_texpos, 0); // Tell the shader were binded on GL_TEXTURE_0



        gl.enable(gl.DEPTH_TEST);
        gl.enable(gl.CULL_FACE );


        nix::registry.set_not_exists('/nix/gl/sprite_buffer_size', 64);
        
        let i_sprite_buffer_size = nix::registry.get('/nix/gl/sprite_buffer_size').value;
        let i_sprite_count       = 2;

        let template_fa_sprite_vertices = [-0.5, -0.5, 0.0,  0.5, -0.5, 0.0,  0.5, 0.5, 0.0,  -0.5, 0.5, 0.0];
        let template_fa_sprite_uvs      = [0.0, 0.0,  1.0, 0.0,  1.0, 1.0,  0.0, 1.0];
        let template_ia_sprite_indices  = [0, 1, 2,  0, 2, 3];

        let fa_sprite_vertices = [];
        let fa_sprite_uvs      = [];
        let ia_sprite_indices  = [];

        for(var i = 0; i < i_sprite_buffer_size; ++i) {
            fa_sprite_vertices.push.apply(fa_sprite_vertices, template_fa_sprite_vertices);
            fa_sprite_uvs.push.apply(fa_sprite_uvs, template_fa_sprite_uvs);
            ia_sprite_indices.push.apply(ia_sprite_indices, [0 + (i*4), 1 + (i*4), 2 + (i*4),  0 + (i*4), 2 + (i*4), 3 + (i*4)]);
        }

        let fa_sprite_position = [-2.0, 0.0, 0.0,
                                  -2.0, 0.0, 0.0,
                                  -2.0, 0.0, 0.0,
                                  -2.0, 0.0, 0.0, 
                                  
                                   2.0, 0.0, 0.0,
                                   2.0, 0.0, 0.0,
                                   2.0, 0.0, 0.0,
                                   2.0, 0.0, 0.0];

        // Using the 'normals' as the sprites position //
        let m_sprite_mesh  = new nix::gl::StaticMesh(gl, fa_sprite_vertices, fa_sprite_uvs, ia_sprite_indices, fa_sprite_position);

        // Set up MVP //

        let mvp_model      = new nix::gl::mat4();
        let mvp_view       = new nix::gl::mat4();
        let mvp_projection = new nix::gl::mat4();
        let mvp_camera     = new nix::gl::mat4();

        mvp_model.scale(vec3.fromValues(2.0, 2.0, 1.0));



        function set_up_camera(w, h) {
            let f_aspect = h / w;
          
            mvp_projection.identity();
            mvp_projection.ortho(0.0, 16.0, 0.0, 16.0 * f_aspect, -2.0, 100.0);
            mvp_view.identity();
            mvp_view.translate(vec3.fromValues(8.0, 8.0 * f_aspect, 0.0));

            mvp_camera.steal(mvp_projection);
            mvp_camera.mul(mvp_camera, mvp_view);
            mvp_camera.mul(mvp_camera, mvp_model);

            gl.uniformMatrix4fv(ul_sprite_mvppos, false, mvp_camera.mat);

            nix::io.putf("Reset Camera\n");
        };

        set_up_camera(s_screen.canvas.width, s_screen.canvas.height);
        s_screen.on_resize(function(w, h) {
            set_up_camera(w, h);
        });



        function loop() {
            nix::gl::clear_with_color(gl, 0.0, 0.0, 0.0, 1.0);

            gl.activeTexture(gl.TEXTURE0);
            tex_atlas.bind();

            m_sprite_mesh.bind();
            m_sprite_mesh.controlled_draw(i_sprite_count * 6);

            requestAnimationFrame(loop);
        }

        requestAnimationFrame(loop);
    };

    return SUCCESS;
}));
