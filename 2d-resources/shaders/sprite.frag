#version 300 es

precision mediump float;
uniform sampler2D tex;

out vec4 frag;
in  vec2 uv;

void main() {
    frag = texture(tex, uv);
}
