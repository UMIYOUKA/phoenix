#version 300 es

uniform mat4 mvp;  // Model * View * Projection

layout(location = 0) in vec3 pos;
layout(location = 1) in vec2 uvp;
layout(location = 2) in vec3 svp;
out vec2 uv;

void main() {
    uv   = uvp;
    gl_Position = mvp*vec4(pos + svp, 1.0);
}