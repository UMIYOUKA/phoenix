#version 100

precision mediump float;

uniform sampler2D shadowmap;
uniform sampler2D block;

uniform vec2 poissonDisk[64];

varying vec4  shadowCoord;
varying vec2  uv;
varying float eye_dist;

float fog_max   = 380.0;
float fog_min   = 180.0;
vec3  fog_color = vec3(1.0, 1.0, 1.0);

const float light_max  = 12.0;
const float light_min  = 0.0;
const float rval       = (1.0 / 256.0);
const vec3 light_color = vec3(rval * 247.0, rval * 230.0, rval * 155.0);

float line_width = 0.10;

void main() {
    float visibility = 1.0;

    for(int i = 0; i <16; i++) {
        if(texture2D(shadowmap, shadowCoord.xy + poissonDisk[i]/2000.0).z < shadowCoord.z) {
            visibility -= 0.05;
        }
    }

    float fog_factor = clamp((fog_max - eye_dist) /
                             (fog_max - fog_min), 0.0, 1.0);

    float light_factor = clamp((light_max - eye_dist) /
                         (light_max - light_min), 0.0, 1.0);

    vec3 o_color = texture2D(block, uv).xyz;

    visibility = clamp(visibility + light_factor, 0.0, 1.0);

    /*if(uv.x<line_width||uv.x>1.0-line_width||uv.y<line_width||uv.y>1.0-line_width) {
        o_color = vec3(0.25);
    }*/

    gl_FragColor = vec4(mix((o_color * visibility), fog_color, 1.0 - fog_factor) + (light_color * light_factor / 4.0), 1.0);
}
