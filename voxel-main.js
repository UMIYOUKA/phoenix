
new modulator.Module("main", "main", (function() {
    window.casey = {
        domain : "casey", // Shortcut for rpipe :p

        skybox_enabled   : true,
        mine_level       : 3,
        bloom_level      : 4,
        voxel_build_tool : false
    };

    ::main = function() {
        modulator.lib_load('libraries/perlin.js');

        nix::registry.set('/casey', casey,
                          '/casey/blocks',   null,
                          '/casey/models',   null,
                          '/casey/textures', null);


        nix::io.put('\n' + nix::registry.root.branch() + '\n');

        modulator.load('voxel-resources/scripts/main-menu.js');
    }

    ::voxel_test = function() {
        /*nix.registry.set('/casey/test', 'I\'m a test variable!');

        nix.io.putf('[main.js] Using stuff on nix v$/nix/version$\n');*/

        // nix.io.putf('Test thingy "$/casey/test$"\n');

        // WebGL setup //

        let screen = document.getElementById('screen');
        let gl = screen.context;

        screen.resize(screen.clientWidth, screen.clientHeight)

        nix::input::on_mouse_down(screen, function(e) {
            nix::input::request_mouse_lock(screen);
        });

        nix::input::on_mouse_up(screen, function(e) {
            mine_oct();
        });



        let resources = new nix::ResourceLoader('voxel-resources/');
        resources.use_gl(gl);
        resources.load('resources.list');

        let rpipe = new nix::RenderPipeline(gl);
        
        rpipe.add_stage("SHADOW");
        rpipe.add_stage("PRE_RENDER");
        rpipe.add_stage("RENDER");
        rpipe.add_stage("POST_RENDER");

        rpipe.add_refs(casey);


        let sky_prog = resources.load('shaders/skybox.prog');

        let m4_sky_mvp  = new nix::gl::mat4();
        let m4_sky_view = new nix::gl::mat4();

        let prog_shadow = resources.load('shaders/shadow.prog');
        let g_prog = resources.load('shaders/matrix.prog');




        let oct_size = 256.0;
        let oct_test = new nix::voxel::Octree({x:oct_size,y:oct_size,z:oct_size});
        oct_test.base_leaf.split();

        // Create platform for player to stand on when spawning //
        /*for(let i=0; i < 4; ++i) {
            oct_test.base_leaf.leaves[i].split();

            for(let j=0; j < 4; ++j) {
                oct_test.base_leaf.leaves[i].leaves[j].split();

                for(let k=0; k < 4; ++k) {
                    oct_test.base_leaf.leaves[i].leaves[j].leaves[k].solid = true;
                }
            }
        }*/
       
        var fun_perlin_split = function(oct) {
            if(noise.perlin3((oct.position.x + 64) / 128.0, oct.position.y / 128.0, oct.position.z / 128.0) > 0.3) {
                if(oct.split_level >= 3) {
                    oct.solid = true;
                    return true;
                }
            }
            
            if(oct.split_level > 6) {
                return true;
            } else {
                oct.split();
                for(let c=0; c<8; ++c) {
                    fun_perlin_split(oct.leaves[c]);
                }
                return true;
            }
        };
        
        fun_perlin_split(oct_test.base_leaf);

        let oct_verts = [];
        let oct_norms = [];
        let oct_uvs   = [];
        let oct_indis = [];

        oct_test.base_leaf.generate_mesh(oct_verts, oct_indis, oct_uvs, oct_norms);

        let s_mesh = new nix::gl::StaticMesh(gl, oct_verts, oct_uvs, oct_indis, oct_norms);

        let oct_tex = resources.load("blocks/casey/white.png");
        oct_tex.set_parameters([nix.gl.TEX_PARAM_I, gl.TEXTURE_WRAP_S, gl.REPEAT],
                               [nix.gl.TEX_PARAM_I, gl.TEXTURE_WRAP_T, gl.REPEAT]);

        // Back to GL stuff //

        // mesh.bind();
        let fb_renderbuffer = new nix::gl::Framebuffer(gl, gl.FRAMEBUFFER);
        fb_renderbuffer.bind();
        let t_color = new nix::gl::Texture(gl, gl.TEXTURE_2D);
        t_color.set_parameters([nix::gl::TEX_PARAM_I, gl.TEXTURE_MIN_FILTER, gl.LINEAR],
                               [nix::gl::TEX_PARAM_I, gl.TEXTURE_MAG_FILTER, gl.LINEAR],
                               [nix::gl::TEX_PARAM_I, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE],
                               [nix::gl::TEX_PARAM_I, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE]);
        t_color.image2D(0, gl.RGB, screen.clientWidth, screen.clientHeight, gl.RGB, gl.UNSIGNED_BYTE, null);
        fb_renderbuffer.texture(gl.COLOR_ATTACHMENT0, t_color, 0);

        let t_bright = new nix::gl::Texture(gl, gl.TEXTURE_2D);
        t_bright.set_parameters([nix::gl::TEX_PARAM_I, gl.TEXTURE_MIN_FILTER, gl.LINEAR],
                                [nix::gl::TEX_PARAM_I, gl.TEXTURE_MAG_FILTER, gl.LINEAR],
                                [nix::gl::TEX_PARAM_I, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE],
                                [nix::gl::TEX_PARAM_I, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE]);
        t_bright.image2D(0, gl.RGB, screen.clientWidth, screen.clientHeight, gl.RGB, gl.UNSIGNED_BYTE, null);
        fb_renderbuffer.texture(gl.COLOR_ATTACHMENT1, t_bright, 0);

        gl.drawBuffers([gl.COLOR_ATTACHMENT0, gl.COLOR_ATTACHMENT1]);

        var rbid = gl.createRenderbuffer();
        gl.bindRenderbuffer(gl.RENDERBUFFER, rbid);
        gl.renderbufferStorage(gl.RENDERBUFFER, gl.DEPTH_COMPONENT16, screen.clientWidth, screen.clientHeight);
        gl.bindRenderbuffer(gl.RENDERBUFFER, null);

        gl.framebufferRenderbuffer(gl.FRAMEBUFFER, gl.DEPTH_ATTACHMENT, gl.RENDERBUFFER, rbid);

        let fb_pingpong = [];
        let fb_pingpong_buffer = [];

        for(var i = 0; i < 2; ++i) {
            fb_pingpong[i] = new nix.gl.Framebuffer(gl, gl.FRAMEBUFFER);
            fb_pingpong[i].bind();
            fb_pingpong_buffer[i] = new nix::gl::Texture(gl, gl.TEXTURE_2D);
            fb_pingpong_buffer[i].set_parameters([nix::gl::TEX_PARAM_I, gl.TEXTURE_MIN_FILTER, gl.LINEAR],
                                   [nix::gl::TEX_PARAM_I, gl.TEXTURE_MAG_FILTER, gl.LINEAR],
                                   [nix::gl::TEX_PARAM_I, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE],
                                   [nix::gl::TEX_PARAM_I, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE]);
            fb_pingpong_buffer[i].image2D(0, gl.RGB, screen.clientWidth, screen.clientHeight, gl.RGB, gl.UNSIGNED_BYTE, null);
            fb_pingpong[i].texture(gl.COLOR_ATTACHMENT0, fb_pingpong_buffer[i], 0);
        }

        rpipe.add_refs({
            domain : "framebuffer",

            main : fb_renderbuffer
        });

        g_prog.use();

        let wgl1_poisson_array    = null;
        let wgl1_poisson_location = null;
        if(gl.feature_level != 'webgl2') {
            wgl1_poisson_array = [
                -0.613392,  0.617481,  0.170019, -0.040254, -0.299417,  0.791925,
                0.645680,  0.493210, -0.651784,  0.717887,  0.421003,  0.027070,
                -0.817194, -0.271096, -0.705374, -0.668203,  0.977050, -0.108615,
                0.063326,  0.142369,  0.203528,  0.214331, -0.667531,  0.326090,
                -0.098422, -0.295755, -0.885922,  0.215369,  0.566637,  0.605213,
                0.039766, -0.396100,  0.751946,  0.453352,  0.078707, -0.715323,
                -0.075838, -0.529344,  0.724479, -0.580798,  0.222999, -0.215125,
                -0.467574, -0.405438, -0.248268, -0.814753,  0.354411, -0.887570,
                0.175817,  0.382366,  0.487472, -0.063082, -0.084078,  0.898312,
                0.488876, -0.783441,  0.470016,  0.217933, -0.696890, -0.549791,
                -0.149693,  0.605762,  0.034211,  0.979980,  0.503098, -0.308878,
                -0.016205, -0.872921,  0.385784, -0.393902, -0.146886, -0.859249,
                0.643361,  0.164098,  0.634388, -0.049471, -0.688894,  0.007843,
                0.464034, -0.188818, -0.440840,  0.137486,  0.364483,  0.511704,
                0.034028,  0.325968,  0.099094, -0.308023,  0.693960, -0.366253,
                0.678884, -0.204688,  0.001801,  0.780328,  0.145177, -0.898984,
                0.062655, -0.611866,  0.315226, -0.604297, -0.780145,  0.486251,
                -0.371868,  0.882138,  0.200476,  0.494430, -0.494552, -0.711051,
                0.612476,  0.705252, -0.578845, -0.768792, -0.772454, -0.090976,
                0.504440,  0.372295,  0.155736,  0.065157,  0.391522,  0.849605,
                -0.620106, -0.328104,  0.789239, -0.419965, -0.545396,  0.538133,
                -0.178564, -0.596057];

            wgl1_poisson_location = g_prog.uniform_location("poissonDisk");

            gl.uniform2fv(wgl1_poisson_location, wgl1_poisson_array);
        }

        gl.enable(gl.DEPTH_TEST);
        gl.enable(gl.CULL_FACE);


        let player_mesh  = new nix::gl::CubeMesh(gl);
        let player_model = new nix::gl::mat4();




        // Matrix magic //

        let m4_model      = new nix::gl::mat4();
        let m4_view       = new nix::gl::mat4();
        let m4_projection = new nix::gl::mat4();

        m4_projection.perspective(90.0, screen.clientWidth/screen.clientHeight, 0.1, 500.0);
        //m4_projection.ortho(-80, 80, -80, 80, 0.1, 120);
        m4_view.translate(vec3.fromValues(0.0, -80.0, -32.0));
        m4_view.rotate_x(19);
        m4_view.rotate_y(135);

        let m4_mvp = new nix::gl::mat4(m4_projection);

        m4_mvp.mul(m4_mvp, m4_view );
        m4_mvp.mul(m4_mvp, m4_model);

        gl.uniformMatrix4fv(g_prog.uniform.mvp, false, m4_mvp.mat);

        // Texture Stuff //

        // Shadowbuffer stuff //

        nix::registry.set_not_exists('/nix/gl/shadow_quality', 2048);
        nix::registry.set_not_exists('/nix/gl/enable_shadows', true);

        casey.smap_res        = nix::registry.get('/nix/gl/shadow_quality').value;
        casey.shadows_enabled = nix::registry.get('/nix/gl/enable_shadows').value;


        let t_depth = new nix::gl::Texture(gl, gl.TEXTURE_2D);

        /* Fixes a chrome bug */
        if(gl.feature_level == "webgl2") {
            t_depth.set_parameters([nix::gl::TEX_PARAM_I, gl.TEXTURE_MAG_FILTER  , gl.LINEAR],
                                   [nix::gl::TEX_PARAM_I, gl.TEXTURE_MIN_FILTER  , gl.LINEAR],
                                   [nix::gl::TEX_PARAM_I, gl.TEXTURE_WRAP_S      , gl.CLAMP_TO_EDGE],
                                   [nix::gl::TEX_PARAM_I, gl.TEXTURE_WRAP_T      , gl.CLAMP_TO_EDGE],
                                   [nix::gl::TEX_PARAM_I, gl.TEXTURE_COMPARE_FUNC, gl.LEQUAL                ],
                                   [nix::gl::TEX_PARAM_I, gl.TEXTURE_COMPARE_MODE, gl.COMPARE_REF_TO_TEXTURE]);
        } else {
            t_depth.set_parameters([nix::gl::TEX_PARAM_I, gl.TEXTURE_MAG_FILTER  , gl.LINEAR],
                                [nix::gl::TEX_PARAM_I, gl.TEXTURE_MIN_FILTER  , gl.LINEAR],
                                [nix::gl::TEX_PARAM_I, gl.TEXTURE_WRAP_S      , gl.CLAMP_TO_EDGE],
                                [nix::gl::TEX_PARAM_I, gl.TEXTURE_WRAP_T      , gl.CLAMP_TO_EDGE]);
        }

        t_depth.image2D(0, (gl.feature_level == "webgl2") ? gl.DEPTH_COMPONENT16 : gl.DEPTH_COMPONENT,
                            casey.smap_res, casey.smap_res,
                            gl.DEPTH_COMPONENT, gl.UNSIGNED_SHORT, null);



        let fb_shadows = new nix::gl::Framebuffer(gl, gl.FRAMEBUFFER);
        fb_shadows.texture(gl.DEPTH_ATTACHMENT, t_depth, 0);

        let sb_glid = fb_shadows.glid;



        gl.bindTexture(gl.TEXTURE_2D, null);

        let m4_light_model      = new nix.gl.mat4();
        let m4_light_view       = new nix.gl.mat4();
        let m4_light_projection = new nix.gl.mat4();

        prog_shadow.use();

        let m4_light_mvp = new nix::gl::mat4();

        m4_light_projection.ortho(-140, 140, -140, 140, 0.1, 420);

        function transform_light(x, y, z, rx, ry, rrx) {
            m4_light_view.identity();

            m4_light_view.translate(vec3.fromValues(x, y, z));
            m4_light_view.rotate_x(rx);
            m4_light_view.rotate_y(ry);
            m4_light_view.translate(vec3.fromValues(-16 / 2.0, 0, -16 / 2.0));
            m4_light_view.rotate_y(rrx);

            let vpos = m4_light_view.get_direction().forward;

            g_prog.use();
            gl.uniform3f(g_prog.uniform.sunpos, vpos.x, vpos.y, vpos.z);

            prog_shadow.use();

            m4_light_mvp.steal(m4_light_projection);

            m4_light_mvp.mul(m4_light_mvp, m4_light_view );
            m4_light_mvp.mul(m4_light_mvp, m4_light_model);

            gl.uniformMatrix4fv(prog_shadow.uniform.mvp, false, m4_light_mvp.mat);
        }

        transform_light(-32.0, -50.0, -32.0, 30, 135, 0);

        // gl.drawBuffer(gl.NONE);

        if(gl.checkFramebufferStatus(gl.FRAMEBUFFER) != gl.FRAMEBUFFER_COMPLETE) {
            nix::io.putf('[main.js] Failed to create a depth framebuffer!\n');
        }

        // Input stuffs //

        let cam_rx = 0.0;
        let cam_ry = 0.0;

        let cam_pos = {
            x : 1.0,
            y : 40.0,
            z : 1.0
        };

        let cam_velocity = {
            x : 0.0,
            y : 0.0,
            z : 0.0
        };

        let key_down = [];

        function upload_view() {
            m4_view.identity();
            m4_view.rotate_x(cam_ry);
            m4_view.rotate_y(cam_rx);
            m4_view.translate(vec3.fromValues(-cam_pos.x, -cam_pos.y, -cam_pos.z));

        }

        nix::input::on_mouse_move(screen, function(e, cx, cy, mmx, mmy) {
            cam_rx += (Math.abs(mmx) > 1) ? mmx / 10.0 : 0;
            cam_ry += (Math.abs(mmy) > 1) ? mmy / 10.0 : 0;

            // nix.io.putf('[main.js] rot % %\n', cam_rx, cam_ry);

            upload_view();
        });

        // Draw routines //

        g_prog.use();

        gl.activeTexture(gl.TEXTURE0);
        t_depth.bind();

        let m4_light_bias = mat4.fromValues(0.5, 0.0, 0.0, 0.0,
                                            0.0, 0.5, 0.0, 0.0,
                                            0.0, 0.0, 0.5, 0.0,
                                            0.5, 0.5004, 0.4995, 1.0);

        if(gl.feature_level !== "webgl2") {
            m4_light_bias = mat4.fromValues(0.5, 0.0, 0.0, 0.0,
                                            0.0, 0.5, 0.0, 0.0,
                                            0.0, 0.0, 0.5, 0.0,
                                            0.5, 0.5004, 0.4994, 1.0);
        }

        let m4_depth_bias = new nix::gl::mat4({mat:m4_light_bias});
        m4_depth_bias.mul(m4_depth_bias, m4_light_mvp);

        gl.uniformMatrix4fv(g_prog.uniform.bias, false, m4_depth_bias.mat);
        gl.uniform1i(g_prog.uniform.shadowmap, 0);
        gl.uniform1i(g_prog.uniform.block, 1);

        nix::gl::clear_with_color(gl, 1, 1, 1, 1);
        // mesh.draw();

        // Called by nix internally whenever a resize event is called by JavaScript //
        screen.on_resize(function(w, h) {
            m4_projection.identity();
            m4_projection.perspective(90.0, w/h, 0.1, 500.0);

            gl.bindRenderbuffer(gl.RENDERBUFFER, rbid);
            gl.renderbufferStorage(gl.RENDERBUFFER, gl.DEPTH_COMPONENT16, w, h);
            gl.bindRenderbuffer(gl.RENDERBUFFER, null);

            t_color.image2D(0, gl.RGB, w, h, gl.RGB, gl.UNSIGNED_BYTE, null);
            t_bright.image2D(0, gl.RGB, w, h, gl.RGB, gl.UNSIGNED_BYTE, null);

            for(var i = 0; i < 2; ++i) {
                fb_pingpong_buffer[i].image2D(0, gl.RGB, w, h, gl.RGB, gl.UNSIGNED_BYTE, null);
            }
        });


        let frame = 0;

        oct_test.base_leaf.matrix = new nix.gl.mat4();
       

        rpipe.insert(rpipe.STAGE_SHADOW, function(ref) {
                gl.activeTexture(gl.TEXTURE0);

                gl.bindFramebuffer(gl.FRAMEBUFFER, ref.shadow.shadowbuffer);
                gl.viewport(0, 0, ref.casey.smap_res, ref.casey.smap_res);

                gl.enable(gl.DEPTH_TEST);
                gl.enable(gl.CULL_FACE);

                ref.shadow.program.use();

                gl.clear(gl.DEPTH_BUFFER_BIT);

                if(ref.casey.shadows_enabled) {
                    ref.shadow.light_mvp.steal(ref.shadow.light_projection);
                    ref.shadow.light_mvp.mul  (ref.shadow.light_mvp, ref.shadow.light_view);
                    ref.shadow.light_mvp.mul  (ref.shadow.light_mvp, ref.voxel.octree.base_leaf.matrix);

                    gl.uniformMatrix4fv(ref.shadow.program.uniform.mvp, false, ref.shadow.light_mvp.mat);

                    ref.voxel.mesh.bind();
                    ref.voxel.mesh.draw();

                    ref.shadow.player_mesh.bind();
                    ref.shadow.player_model.identity();
                    ref.shadow.player_model.translate(vec3.fromValues(ref.camera.cam_pos.x,
                                                                      ref.camera.cam_pos.y,
                                                                      ref.camera.cam_pos.z));
                    ref.shadow.player_model.rotate_x(ref.camera.camera_rotation_y);
                    ref.shadow.player_model.rotate_y(-ref.camera.camera_rotation_x);

                    ref.shadow.light_mvp.steal(ref.shadow.light_projection);
                    ref.shadow.light_mvp.mul  (ref.shadow.light_mvp, ref.shadow.light_view);
                    ref.shadow.light_mvp.mul  (ref.shadow.light_mvp, ref.shadow.player_model);

                    gl.uniformMatrix4fv(ref.shadow.program.uniform.mvp, false, ref.shadow.light_mvp.mat);

                    ref.shadow.player_mesh.draw();
                }
            }, {
                domain : "shadow",

                shadowbuffer : sb_glid,
                program      : prog_shadow,

                light_projection : m4_light_projection,
                light_mvp        : m4_light_mvp,
                light_view       : m4_light_view,

                player_mesh  : player_mesh,
                player_model : player_model,

                camera_position   : cam_pos,
                camera_rotation_x : cam_rx,
                camera_rotation_y : cam_ry
            }
        );

        rpipe.insert(rpipe.STAGE_PRE_RENDER, function(ref) {
                gl.viewport(0, 0, ref.prerender.screen.clientWidth, ref.prerender.screen.clientHeight);
                ref.framebuffer.main.bind();

                gl.clear(gl.DEPTH_BUFFER_BIT | gl.COLOR_BUFFER_BIT);
            }, {
                domain : "prerender",

                screen : screen
            }
        );

        //do_shadows();


        let matrix_callback = function(chunk) {
            m4_mvp.steal(m4_projection       );
            m4_mvp.mul  (m4_mvp, m4_view     );
            m4_mvp.mul  (m4_mvp, chunk.matrix);

            gl.uniformMatrix4fv(g_prog.uniform.mvp, false, m4_mvp.mat);
            gl.uniform3f(g_prog.uniform.cpos, chunk.position.x, chunk.position.y, chunk.position.z);
        };



        let prog_blur = resources.load("shaders/blur.prog");
        prog_blur.use();
        gl.uniform1i(prog_blur.uniform.horizontal, 1);
        gl.uniform1i(prog_blur.uniform.tex, 0);

        let prog_fbdraw = resources.load("shaders/basic.prog");

        let vao_scr = new nix::gl::VAO(gl);
        prog_fbdraw.use();
        gl.uniform1i(prog_fbdraw.uniform.tex, 0);
        gl.uniform1i(prog_fbdraw.uniform.bright, 1);
        vao_scr.bind();
        let buf_scr_verts = new nix::gl::Buffer(gl, new Float32Array([-1.0,-1.0, 1.0,-1.0, 1.0,1.0, -1.0,1.0]), gl.ARRAY_BUFFER, gl.STATIC_DRAW);
        let buf_scr_ind   = new nix::gl::Buffer(gl, new Int32Array([0,1,2, 2,3,0]), gl.ELEMENT_ARRAY_BUFFER, gl.STATIC_DRAW);
        gl.enableVertexAttribArray(0);

        buf_scr_verts.bind();
        gl.vertexAttribPointer(0, 2, gl.FLOAT, false, 0, 0);
        buf_scr_ind.bind();

        let prog_wire = resources.load("shaders/wire.prog");
        prog_wire.use();
        gl.uniform1i(prog_wire.uniform.fb_in, 0);

        let m4_voxel_model = new nix.gl.mat4();

        function mine_oct() {
            //nix.io.putf("Mining octree\n");

            let pos = vec3.create();
            m4_voxel_model.get_translation(pos);
            let oct_pos = {x:pos[0],y:pos[1],z:pos[2]};

            let leaf = oct_test.base_leaf.find_leaf(oct_pos);
            let o_leaf = leaf;

            if(!leaf) {
                nix.io.putf("Failed to find a leaf at %\n", pos);
                return;
            }

            if(casey.voxel_build_tool && leaf.solid) {
                nix.io.putf("Leaf isn't solid! Cannot mine.\n");
                return;
            }

            while(leaf && leaf.split_level < casey.mine_level) {
                let solid = leaf.solid;
                leaf.split();

                for(let lc of leaf.leaves) {
                    lc.solid = !casey.voxel_build_tool && solid;
                }

                //nix.io.putf("Leaf level : %\n", leaf.split_level);

                o_leaf = leaf;
                leaf = leaf.find_leaf(oct_pos);
            }

            if(!leaf) {
                leaf = o_leaf;
                while(leaf && leaf.split_level > casey.mine_level) {
                    leaf = leaf.parent;
                    leaf.leaves = [];
                }
                
                leaf.solid = casey.voxel.voxel_build_tool;
            } else {
                while(leaf.split_level > casey.mine_level) {
                    leaf = leaf.parent;
                    leaf.leaves = [];
                }

                leaf.solid = casey.voxel_build_tool;
            }

            s_mesh.destroy();
            oct_verts = [];
            oct_uvs   = [];
            oct_indis = [];
            oct_norms = [];
            oct_test.base_leaf.generate_mesh(oct_verts, oct_indis, oct_uvs, oct_norms);
            s_mesh = new nix::gl::StaticMesh(gl, oct_verts, oct_uvs, oct_indis, oct_norms);
            rpipe.get_refs("voxel").mesh = s_mesh;
        }

        let jumping = false;

        gl.enable(gl.BLEND);
        gl.blendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA);

        rpipe.insert(rpipe.STAGE_RENDER, (function(ref) {
                // Stage for rendering the terrain //
                ref.voxel.program.use();
                ref.voxel.matrix_callback(ref.voxel.octree.base_leaf);

                gl.activeTexture(gl.TEXTURE0);
                ref.voxel.depth_texture.bind();

                gl.activeTexture(gl.TEXTURE1);
                ref.voxel.octree_texture.bind();

                ref.voxel.mesh.bind();

                if(!ref.casey.wireframe) {
                    ref.voxel.mesh.draw();
                } else {
                    gl.drawElements(gl.LINE_STRIP, ref.voxel.mesh.index_length-1, gl.UNSIGNED_INT, 0);
                }

                gl.uniform3f(ref.voxel.program.uniform.eyepos,
                             ref.camera.cam_pos.x, ref.camera.cam_pos.y, ref.camera.cam_pos.z);

                ref.voxel.upload_view();

            }), {
                domain : "voxel",

                mesh    : s_mesh,
                program : g_prog,

                cam_pos     : cam_pos,      // Passthroughs just to get rpipe fully integrated //
                upload_view : upload_view, /////////////////////////////////////////////////////

                depth_texture  : t_depth,
                octree_texture : oct_tex,

                matrix_callback : matrix_callback, // Likely to get merged

                octree : oct_test
            }
        );

        rpipe.insert(rpipe.STAGE_RENDER, (function(ref) {

        }), {
            domain : "skybox"
        });

        if(rpipe.compile() != SUCCESS) {
            nix::io.putf("[main] Renderpipe failed to compile! Cannot continue!\n");
            return FAIL;
        }

        nix::io.putf("[main] Pipeline function\n%\n", rpipe.compiled_pipeline);

        function loop() {
            // mesh.bind();

            let cdir = null;
            let ldir = null;

            cdir = (nix::input.get_key(screen, 'w')) ? m4_view.get_direction().forward : (nix::input.get_key(screen, 's')) ? m4_view.get_direction().backward : {x:0,y:0,z:0};
            ldir = (nix::input.get_key(screen, 'a')) ? m4_view.get_direction().left    : (nix::input.get_key(screen, 'd')) ? m4_view.get_direction().right    : {x:0,y:0,z:0};

            cam_pos.x += (cdir.x + ldir.x) / 4.0;
            //cam_pos.y += (cdir.y + ldir.y) / 4.0;
            cam_pos.z += (cdir.z + ldir.z) / 4.0;

            let floor = oct_test.base_leaf.find_leaf(Math.vecAdd(cam_pos, {x:0, y:-2, z:0}));
            if(!floor || !floor.solid) {
                cam_pos.y -= 0.2;
                cam_velocity.y -= 0.01;
            } else {
                if(floor) {
                    cam_velocity.y = 0;
                    jumping = false;
                }
            }

            if(nix.input.get_key(screen, ' ') && !jumping) {
                cam_velocity.y += 0.5;
                jumping = true;
            }

            cam_pos = Math.vecAdd(cam_pos, cam_velocity);

            // nix.io.putf("% % %\n", cdir.x, cdir.y, cdir.z);

            rpipe.add_refs({
                domain : "camera",

                cam_pos           : cam_pos,
                camera_rotation_x : cam_rx,
                camera_rotation_y : cam_ry
            });

            // Render the whole scene //
            rpipe.run();

            let d_forward = m4_view.get_direction().forward;
            let v_size    = oct_size / Math.pow(2, casey.mine_level);
            let v_pos     = vec3.create();

            for(var dst = 1; dst < 10; ++dst) {
                m4_voxel_model.identity();
                m4_voxel_model.translate(vec3.fromValues((Math.floor((cam_pos.x + (d_forward.x*v_size*dst))/v_size)*v_size),
                                                         (Math.floor((cam_pos.y + (d_forward.y*v_size*dst))/v_size)*v_size),
                                                         (Math.floor((cam_pos.z + (d_forward.z*v_size*dst))/v_size)*v_size)));

                m4_voxel_model.get_translation(v_pos);
                let leaf = oct_test.base_leaf.find_leaf({x:v_pos[0], y:v_pos[1], z:v_pos[2]});
                if(leaf && leaf.solid) {
                    if(casey.voxel_build_tool) {
                        m4_voxel_model.identity();
                        m4_voxel_model.translate(vec3.fromValues((Math.floor((cam_pos.x + (d_forward.x*v_size*(dst-1)))/v_size)*v_size),
                                                                (Math.floor((cam_pos.y + (d_forward.y*v_size*(dst-1)))/v_size)*v_size),
                                                                (Math.floor((cam_pos.z + (d_forward.z*v_size*(dst-1)))/v_size)*v_size)));
                    }
                    break;
                }
            }
            m4_voxel_model.scale(vec3.fromValues(v_size, v_size, v_size));
            m4_voxel_model.translate(vec3.fromValues(0.5, 0.5, 0.5));

            m4_mvp.steal(m4_projection   );
            m4_mvp.mul  (m4_mvp, m4_view );
            m4_mvp.mul  (m4_mvp, m4_model);






            if(casey.skybox_enabled) {
                gl.disable(gl.CULL_FACE);

                m4_sky_mvp.steal(m4_projection);
                m4_sky_view.identity();
                m4_sky_view.rotate_x(cam_ry);
                m4_sky_view.rotate_y(cam_rx);
                m4_sky_mvp.mul(m4_sky_mvp, m4_sky_view);

                sky_prog.use();

                gl.uniformMatrix4fv(sky_prog.uniform.mvp, false, m4_sky_mvp.mat);

                player_mesh.bind();
                player_mesh.draw();

                gl.enable(gl.CULL_FACE);
            }

            //do_shadows();

            //gl.disable(gl.CULL_FACE);
            //gl.disable(gl.DEPTH_TEST);

            vao_scr.bind();

            fb_pingpong[0].bind();
            prog_blur.use();
            gl.uniform1i(prog_blur.uniform.horizontal, 1);
            gl.activeTexture(gl.TEXTURE0);
            t_bright.bind();
            gl.drawElements(gl.TRIANGLES, 6, gl.UNSIGNED_INT, 0);

            let bl_horizontal = true;

            for(let bl=0; bl<casey.bloom_level; ++bl) {
                let curr_fb = bl_horizontal ? 1 : 0; // Dumb thing needed for JS //

                fb_pingpong[curr_fb].bind();
                gl.uniform1i(prog_blur.uniform.horizontal, curr_fb);
                fb_pingpong_buffer[bl_horizontal ? 0 : 1].bind();
                gl.drawElements(gl.TRIANGLES, 6, gl.UNSIGNED_INT, 0);
                bl_horizontal = !bl_horizontal;
            }

            gl.bindFramebuffer(gl.FRAMEBUFFER, null);
            gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
            prog_fbdraw.use();

            gl.activeTexture(gl.TEXTURE0);
            t_color.bind();
            gl.activeTexture(gl.TEXTURE1);
            fb_pingpong_buffer[0].bind();

            gl.drawElements(gl.TRIANGLES, 6, gl.UNSIGNED_INT, 0);

            prog_wire.use();
            m4_mvp.steal(m4_projection         );
            m4_mvp.mul  (m4_mvp, m4_view       );
            m4_mvp.mul  (m4_mvp, m4_voxel_model);
            //m4_mvp.mul  (m4_mvp, chunk.matrix);
            gl.uniformMatrix4fv(prog_wire.uniform.mvp, false, m4_mvp.mat);
            gl.uniform1f(prog_wire.viewport_width, screen.width);

            gl.disable(gl.DEPTH_TEST);
            player_mesh.bind();
            //gl.drawElements(gl.LINE_STRIP, player_mesh.index_length-1, gl.UNSIGNED_INT, 0);
            player_mesh.draw();
            gl.enable(gl.DEPTH_TEST);

            frame++;
            requestAnimationFrame(loop);
        };

        requestAnimationFrame(loop);
    }

    return SUCCESS;
}));
