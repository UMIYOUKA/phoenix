/*
 * PHOENIX quality - of - understanding - what - any - of - this - code - means - life
 */

var SUCCESS      = 1;
var FAIL         = 0;
var UNDETERMINED = 2;
var UNKNOWN      = 3;

var ERROR_MESSAGE = "No error reported";
var ERROR_CODE    = UNDETERMINED;
var PHOENIX_MAIN  = document.currentScript.getAttribute('main') || 'main.js';


/*
 * PHOENIX Module loading & base class
 * - Module system allows stuff to be loaded at runtime so you don't have to manually add <script> elements
 * - Makes modding stuff a whole lot easier since you can just load a module instead of injecting scripts
 */

/*
 * Base Object
 * - Init system sets up the very basics needed to load the default modules
 * - Modules is the only pre defined subsystem, since the others will be loaded as modules
 * - Yes, you can disable critical modules, why wouldn't you be able to?
 * - Once init is done we start using nix.cout instead of nix.init.log
 */

let phoenix = {
    _error_message : "",

    file : {
        initialize : null,

        load : null,
        json : null
    },

    version : {
        major : 0,
        minor : 8,
        patch : 12,

        get all() {
            return this.major + "." + this.minor + "-" + this.patch;
        }
    },

    init : {
        _has_error : false,

        start   : null,
        banner  : "",
        message : function(module, message, level) {
            console.log("[" + module + "] (" + level + ") " + message);
        },
        log     : function(module, message) {
            phoenix.init.message(module, message, "info");
        },
        error   : function(module, message) {
            phoenix.init.message(module, message, "error");
        },
        warn    : function(module, message) {
            phoenix.init.message(module, message, "warning");
        }
    }
};

// let nix = phoenix;

let nice = "https://reddit.com/r/me_irl\n";

/*
 * Modulator - A replacement for phoenix.modules
 */

var modulator = {
    Module : null,

    modules    : [],
    namespaces : {},

    path_segments : [],

    current_path : function() {
        let s_out = "";

        for(s_seg of this.path_segments)
            s_out += s_seg;
        
        return s_out;
    },

    load : function(s_path, b_autonamespace) {
        for(let i=0; i<arguments.length; i+=2) {
            let s_realpath = this.current_path() + arguments[i];
            this.path_segments.push(arguments[i].substring(0, arguments[i].lastIndexOf('/') + 1));

            var s_data = phoenix.file.load(s_realpath).replace(/(?:\n|(\n\s+))(:{2})/g, '\nthis.').replace(/(?:\s+)(:{2})/g, ' this.').replace(/:{2}/g, '.');
            var m_current = eval(s_data);

            if(m_current === undefined) {
                phoenix.init.error('modulator', 'Failed loading module "' + arguments[i] + '", No module found!');
                this.path_segments.pop();
                return FAIL;
            }

            this.modules.push(m_current);
            if(arguments[i + 1]) this.use(m_current.s_namespace);

            this.path_segments.pop();
            if(m_current.state != SUCCESS) {
                return m_current.sate;
            }
        }

        return SUCCESS;
    },

    lib_load : function(s_path) {
        // Cannot use namespaces when loading a JS library since it
        // is a <script> appended to document.body

        let s_realpath = this.current_path() + s_path;

        let obj  = document.createElement('script');
        obj.type = "text/javascript";
        obj.text = phoenix.file.load(s_realpath);

        document.body.appendChild(obj);
    },

    use : function(s_namespace) {
        /*let mod = modules.find(function(s_mod) {
            return s_name == s_mod.s_name;
        });*/

        phoenix.init.log('modulator', 'Using namespace "' + s_namespace + '"');

        if(!this.namespaces[s_namespace]) {
            phoenix.init.error('modulator', 'Failed using namespace "' + s_namespace + '"   Reason : Namespace not found!');
            return FAIL;
        }

        window[s_namespace] = this.namespaces[s_namespace];

        return SUCCESS;
    }
};

modulator.Module = class _internal_class_modulator_Module {
    constructor(as_name, as_namespace, af_initializer) {
        this.state = UNDETERMINED;
        this.create(as_name, as_namespace, af_initializer);
    }

    create(as_name, as_namespace, af_initializer) {
        this.s_namespace = as_namespace;
        this.s_name      = as_name;
        this.f_init      = af_initializer;

        phoenix.init.log('modulator', 'Initializing module "' + this.s_name + '"');

        if(!modulator.namespaces[this.s_namespace]) {
            modulator.namespaces[this.s_namespace] = {};
        }

        if(this.f_init.bind(modulator.namespaces[this.s_namespace], this)(modulator.namespaces[this.s_namespace]) != SUCCESS) {
            phoenix.init.error('modulator', '(' + ERROR_CODE + ') Failed initializing module "' + this.s_name + '"   Reason : ' + ERROR_MESSAGE);
            this.state = FAIL;
            return FAIL;
        }

        phoenix.init.log('modulator', 'Finished initializing module "' + this.s_name + '"');

        this.state = SUCCESS;
        return SUCCESS;
    }

    /*use() {
        phoenix.init.log('modulator', 'Using module "' + this.s_name + '" in namespace "' + this.s_namespace + '"');

        if(window[this.s_name]) {
            phoenix.init.error('modulator', '(' + ERROR_CODE + ') Failed using module "' + this.s_name + '"   Reason : Name already exists in window[]!');
            return FAIL;
        }

        window[this.s_name] = this;
        return SUCCESS;
    }*/
};

/*
 * Phoenix File Initialization
 * - Introduces the needed functions for phoenix.modules and others
 */

phoenix.file.initialize = function() {
    phoenix.file.load = function(path) {
        let sync_obj = new XMLHttpRequest();
        sync_obj.overrideMimeType("text");
        sync_obj.open('GET', path, false);
        try {
            sync_obj.send('');
        } catch(e) {
            if(typeof nix !== "undefined") {
                nix.io.putf('[nix::file::load] Cannot load file "%"\n[nix::file::load] Reason : %\n',
                            path, sync_obj.statusText);
            } else {
                phoenix.init.error('nix::file::load', 'Cannot load file "' + path + '"   Reason : ' + sync_obj.statusText + '');
            }

            return null;
        }

        return sync_obj.response;
    };

    phoenix.file.json = function(path) {
        return JSON.parse(phoenix.file.load(path));
    }

    return true;
};

// Use the functions from phoenix.file as a placeholder for a real file class //
function bind_nix_file() {
    nix.file = phoenix.file;
}

/*
 * Phoenix Initialization
 * - Start up the 'modules' system
 * - Load default modules (TODO)
 * - Handle module loading errors
 */

phoenix.init.banner = `<b>
<span style='color:#FF0000'>    ;;;;;  ;' ;' ;;;    ;;;;' ;   ;' .;;;: ;  .;</span>
<span style='color:#EE0000'>   ;'  ;  ;' ;' ;' ;   ;'    ;:  ;'   ;'   :.;'</span>
<span style='color:#CC1100'>  ;;;;'  ;;;;' ;' .;  ;;;;' ;': ;'   ;'   .:'</span>
<span style='color:#995500'> ;'     ;' ;'  ; .;' ;'    ;' :;'   ;'  .;':</span>
<span style='color:#669900'>;'     ;' ;'   ;;'  ;;;;' ;'  ;' ';;;: .'  ;</span> v`+ phoenix.version.all +`
</b>
using browser `+ window.navigator.userAgent +`
`;

phoenix.init.start = function() {
    phoenix.init.log("init", "Starting PHOENIX v" + phoenix.version.all);

    let t0 = performance.now();

    if(!phoenix.file.initialize()) return false;
    // Force WebGL 1 if needed for testing //
    //phoenix.registry.set('nix/gl');
    // phoenix.registry.set('nix/gl/force_webgl1', true);
    if(!modulator.load('nix/nix.js', true))      return false;

    bind_nix_file();

    phoenix.init.message = (function(module, msg, level) {
        nix.io.putf('<b><span style="color:#46D6B7">[%]</span> <span style="color:#EDD24B">(%)</span></b> %\n', module, level, msg);
    });

    let t1 = performance.now();

    /*var io = new m_s.TextStream();
    io.add_listener((function(s_message) {
        console.log(s_message);
    }));*/

    nix.io.putf("[phoenix::init::start()] Finished initialization! (time %ms)\n", Math.round(t1 - t0));

    nix.io.put(phoenix.init.banner);

    modulator.load(PHOENIX_MAIN, true);

    main.main();
}

phoenix.init.start();
